module Widgets.ReportError where

import qualified Data.Text      as T
import           Graphics.UI.WX

import           Config

reportError :: Window a -> String -> IO ()
reportError w msg = errorDialog w "Greška" (formatMessage msg)
                 >> error msg
    where formatMessage msg = "Dogodila se greška:\n\n\""
                       ++ msg
                       ++ "\"\n\nPrijaviti na " ++ T.unpack authorEmail
