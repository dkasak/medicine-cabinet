module Widgets.TypedList where

import           Control.Monad
import           Data.List
import           Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as N
import           Data.Maybe
import qualified Data.Text          as T
import           Graphics.UI.WX     hiding (sorted, when)
import           Graphics.UI.XTC    hiding (ListView)

import           Classes
import           Controls
import           Utils

data TypedListFilter a = TypedListFilter
    { typedListFilterName    :: T.Text
    , typedListFilter        :: T.Text -> a -> Bool
    , typedListSortFunc      :: a -> a -> Ordering
    , typedListSortDirection :: Sort
    }

data TypedListMeta a = TypedListMeta
    { typedListColumns     :: [ColumnDesc]                 -- table columns
    , typedListFilters     :: NonEmpty (TypedListFilter a) -- search filters
    , typedListDisplayRow  :: a -> [String]                -- column values extraction function
    , typedListFetchValues :: IO [a]                       {- action that fetches current list of
                                                              values (e.g. from a database)
                                                           -}
    }

data TypedList a = TypedList
    { typedListMeta     :: TypedListMeta a
    , typedListView     :: ListView a
    , typedListSelector :: ChoiceView (TypedListFilter a) ()
    , typedListSearch   :: ValueEntry T.Text ()
    , typedListLayout   :: Layout
    }

instance Label (TypedListFilter a) where
    labelled = T.unpack . typedListFilterName

data Sort = Ascending | Descending

-- Creates a widget that served as a list of typed values, implemented
-- using WX's ListView. Enables searching and sorting by certain columns
typedList :: Window w
          -> TypedListMeta a
          -> IO (TypedList a)
typedList w tlm = do
    let defaultFilter = N.head $ typedListFilters tlm
    let fetchValues = typedListFetchValues tlm

    searchBox   <- textField w
    selectorBox <- mkChoiceView w []
    set selectorBox [typedItems := N.toList $ typedListFilters tlm,
                     typedMaybeSelection := Just defaultFilter]

    lv <- listView' w (typedListColumns tlm) (typedListDisplayRow tlm)

    items <- fetchValues
    listViewSetItems lv $ sorted defaultFilter items

    let layout = column 5 [ row 2 [widget selectorBox, hfill $ widget searchBox]
                          , fill $ listViewLayout lv]
        tl = TypedList tlm lv selectorBox searchBox layout

    set selectorBox [on select := do searchTerm <- typedListSearchTerm searchBox
                                     filter <- typedListActiveFilter tl
                                     rows <- typedListFilteredRows fetchValues filter searchTerm
                                     listViewSetItems lv rows]
    set searchBox [on update := void $ filterTypedList tl]

    return tl

typedListSearchTerm :: Textual w => w -> IO T.Text
typedListSearchTerm searchBox = T.pack <$> get searchBox text

typedListFilteredRows :: IO [a]
                      -> TypedListFilter a
                      -> T.Text
                      -> IO [a]
typedListFilteredRows fetchValues flt searchTerm
  | T.null searchTerm =
    do items <- fetchValues
       return $ sorted flt items
  | otherwise =
    do items <- fetchValues
       let f = typedListFilter flt
           items' = filter (f searchTerm) items
       return $ sorted flt items'

sorted :: TypedListFilter a -> [a] -> [a]
sorted flt items =
    case typedListSortDirection flt of
        Ascending  -> ascendingSort items
        Descending -> descendingSort items
    where sortFunc = typedListSortFunc flt
          ascendingSort = sortBy sortFunc
          descendingSort = sortBy (reversed .: sortFunc)
          (.:) = (.).(.)
          reversed EQ = EQ
          reversed LT = GT
          reversed GT = LT

filterTypedList :: TypedList a -> IO [a]
filterTypedList tl =
    do flt <- typedListActiveFilter tl
       searchTerm <- typedListSearchTerm searchBox
       filterTypedListByColumnAndTerm lv fetchValues flt searchTerm
    where fetchValues = typedListFetchValues . typedListMeta $ tl
          lv = typedListView tl
          searchBox = typedListSearch tl

filterTypedListByColumnAndTerm :: ListView a
                               -> IO [a]
                               -> TypedListFilter a
                               -> T.Text
                               -> IO [a]
filterTypedListByColumnAndTerm lv fetchValues flt searchTerm =
    do rows <- typedListFilteredRows fetchValues flt searchTerm
       listViewSetItems lv rows
       return rows

typedListActiveFilter :: TypedList a
                      -> IO (TypedListFilter a)
typedListActiveFilter tl = liftM (fromMaybe defaultFilter) (get selectorBox typedMaybeSelection)
    where defaultFilter = N.head . typedListFilters . typedListMeta $ tl
          selectorBox = typedListSelector tl

