{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE ScopedTypeVariables       #-}

module Widgets.Form (Form, FormField(..),
                     textField, valueField,
                     form, setFormLayout, showForm,
                     formAcceptButton, formCloseButton) where

import Control.Monad              (forM_, liftM2)
import Data.Maybe                 (isJust)
import Data.Monoid                ((<>))
import Graphics.UI.WX             hiding (Form)
import Graphics.UI.XTC            (ChoiceView, ValueEntry, typedMaybeSelection,
                                   typedValue)

import Controls                   (textField, valueField)
import Graphics.UI.WX.Combinators

data FormField = forall x. ValueField
    { valueControl :: ValueEntry x ()
    , valueLabel   :: String
    }
    | forall x. ChoiceField
    { choiceControl :: ChoiceView x ()
    , choiceLabel   :: String
    }

type Form = Dialog ()

form :: Panel () -> IO Form
form f = dialog f []

showForm :: Form -> Button () -> Button () -> IO (Maybe ())
showForm f submit close = showModal f (liftM2 (>>) (acceptOn submit) (closeOn close))

closeOn :: Button a -> (Maybe () -> IO ()) -> IO ()
closeOn b returning = set b [on command := returning Nothing]

acceptOn :: Button a -> (Maybe () -> IO ()) -> IO ()
acceptOn b returning = set b [on command := returning $ Just ()]

formAcceptButton :: Bool -> Form -> [FormField] -> IO (Button ())
formAcceptButton initiallyEnabled f fields =
    button f [text := "Unesi", enabled := initiallyEnabled] >>=
    enabledWhenFormFields validNonEmpty fields

formCloseButton :: Form -> IO (Button ())
formCloseButton f = button f [text := "Zatvori"]

fieldToLayouts :: FormField -> [Layout]
fieldToLayouts (ValueField f l) = [label (l <> ":"), space 5 0, alignRight . hfill $ widget f]
fieldToLayouts (ChoiceField f l) = [label (l <> ":"), space 5 0, alignRight . hfill $ widget f]

setFormLayout :: String -> Form -> [FormField] -> Size -> Button () -> Button () -> IO ()
setFormLayout name f fields size submit close =
    set f [layout := minsize size
                   $ formLayout name
                                fields
                                (widget submit)
                                (widget close)]

formLayout :: String -> [FormField] -> Layout -> Layout -> Layout
formLayout name fields submit close =
    floatBottom . stretch . fill $
    marginWidth 5 . marginBottom . marginLeft . marginRight $
    column 5
    [ fill . boxed name . margin 5 . grid 2 2 $ widgets
    , glue
    , floatBottom $ row 5 [hfill submit, hfill close]]
    where widgets = map fieldToLayouts fields

validNonEmpty' :: FormField -> IO Bool
validNonEmpty' (ValueField f _) = (&&) <$> valid <*> nonempty
    where valid = isJust <$> get f typedValue
          nonempty = not . null <$> get f text
validNonEmpty' (ChoiceField f _) = isJust <$> get f typedMaybeSelection

validNonEmpty :: [FormField] -> IO Bool
validNonEmpty = fmap and . mapM validNonEmpty'

setFormFieldUpdateHandler :: IO () -> FormField -> IO ()
setFormFieldUpdateHandler h (ValueField f _)  = setUpdateHandler h f
setFormFieldUpdateHandler h (ChoiceField f _) = setSelectHandler h f

enabledWhenFormFields :: ([FormField] -> IO Bool) -> [FormField] -> Window a -> IO (Window a)
enabledWhenFormFields p fields w = forM_ fields (setFormFieldUpdateHandler toggleWidget) >> return w
  where toggleWidget = p fields >>= (\state -> set w [enabled := state])
