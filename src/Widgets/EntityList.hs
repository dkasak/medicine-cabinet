{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Widgets.EntityList where

import           Control.Applicative
import           Control.Monad        (liftM, void, when)
import           Data.Maybe
import qualified Data.Text            as T
import           Database.Persist     hiding (get, update)
import           Database.Persist.Sql (SqlBackend)
import           Graphics.UI.WX       hiding (when)
import           Graphics.UI.XTC      hiding (ListView)
import           Safe

import           Controls
import           Database             (runDB, selectAll, (=~.))
import           Utils

data EntityList a = EntityList
  { entityListColumns    :: [ColumnDesc]            -- table columns
  , entityListFilters    :: [EntityField a T.Text]  -- search filters
  , entityListDisplayRow :: Entity a -> [String]    -- column values extraction function
  }

-- Creates an entity list widget for a database table (a Persistent Entity).
-- Displays existing table entries inside a list view (with customizable
-- columns) and enables searching and sorting by certain columns
-- (implemented on the database side).
entityList :: (Labeled (EntityField a T.Text),
               PersistEntityBackend a ~ SqlBackend,
               PersistEntity a)
           => GuiContext
           -> EntityList a
           -> Var [Entity a]
           -> IO (ListView (Entity a),
                  ChoiceView (EntityField a T.Text) (),
                  ValueEntry T.Text (),
                  Layout)
entityList ctx v rowsVar = do
    let defaultColumn = headMay $ entityListFilters v
    let filters = fmap Asc . maybeToList $ defaultColumn
    rows <- runDB $ selectList [] filters

    set rowsVar [value := rows]

    searchBox   <- textField (guiPanel ctx)
    selectorBox <- mkChoiceView (guiPanel ctx) []
    set selectorBox [typedItems := entityListFilters v,
                     typedMaybeSelection := defaultColumn]

    lv <- listView' (guiPanel ctx) (entityListColumns v) (entityListDisplayRow v)
    listViewSetItems lv rows

    set selectorBox [on select := do searchTerm <- entityListSearchTerm searchBox
                                     mfilter <- entityListActiveFilter v selectorBox
                                     rows <- entityListFilteredRows mfilter searchTerm
                                     when (T.null searchTerm) $
                                        set rowsVar [value := rows]
                                     listViewSetItems lv rows]
    set searchBox [on update := void $ filterList v lv selectorBox searchBox rowsVar]

    return (lv, selectorBox, searchBox,
                column 5 [ row 2 [widget selectorBox, hfill $ widget searchBox]
                         , fill $ listViewLayout lv])

entityListActiveFilter :: TypedMaybeSelection (EntityField a T.Text) w
                       => EntityList a
                       -> w
                       -> IO (Maybe (EntityField a T.Text))
entityListActiveFilter v selectorBox = liftM (<|> defaultFilter) (get selectorBox typedMaybeSelection)
    where defaultFilter = headMay $ entityListFilters v

entityListSearchTerm :: Textual w => w -> IO T.Text
entityListSearchTerm searchBox = T.pack <$> get searchBox text

entityListFilteredRows :: (PersistEntity val,
                           PersistEntityBackend val ~ SqlBackend)
                       => Maybe (EntityField val T.Text)
                       -> T.Text
                       -> IO [Entity val]
entityListFilteredRows (Just column) searchTerm = runDB $ selectList [column =~. searchTerm] [Asc column]
entityListFilteredRows Nothing _ = runDB selectAll

filterList :: (PersistEntity val,
               Textual w1,
               TypedMaybeSelection (EntityField val T.Text) w,
               PersistEntityBackend val ~ SqlBackend)
           => EntityList val
           -> ListView (Entity val)
           -> w
           -> w1
           -> Var [Entity val]
           -> IO [Entity val]
filterList v lv selectorBox searchBox rowsVar =
    do column <- entityListActiveFilter v selectorBox
       searchTerm <- entityListSearchTerm searchBox
       filterListByColumnAndTerm lv column searchTerm rowsVar

filterListForce :: (PersistEntity val,
                    Textual w1,
                    TypedMaybeSelection (EntityField val T.Text) w,
                    PersistEntityBackend val ~ SqlBackend)
                => EntityList val
                -> ListView (Entity val)
                -> w
                -> w1
                -> Var [Entity val]
                -> IO [Entity val]
filterListForce v lv selectorBox searchBox _ =
    do column <- entityListActiveFilter v selectorBox
       searchTerm <- entityListSearchTerm searchBox
       rows <- entityListFilteredRows column searchTerm
       listViewSetItems lv rows
       return rows

filterListByColumnAndTerm :: (PersistEntity val,
                              Valued w,
                              PersistEntityBackend val ~ SqlBackend)
                          => ListView (Entity val)
                          -> Maybe (EntityField val T.Text)
                          -> T.Text
                          -> w [Entity val]
                          -> IO [Entity val]
filterListByColumnAndTerm lv mcolumn searchTerm rowsVar =
    do rows <- if not (T.null searchTerm)
                 then entityListFilteredRows mcolumn searchTerm
                 else get rowsVar value
       listViewSetItems lv rows
       return rows
