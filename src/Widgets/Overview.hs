{-# LANGUAGE ScopedTypeVariables #-}

module Widgets.Overview where

import           Control.Monad              (liftM, unless, when, (>=>))
import           Data.Maybe
import qualified Data.Text                  as T
import           Database.Persist           hiding (get, update)
import           Database.Persist.Sql       (SqlBackend)
import           Graphics.UI.WX             hiding (Key, when)
import           Graphics.UI.WX.Combinators (enabledWhen)
import           Graphics.UI.XTC            hiding (ListView)
import           Safe

import           Controls.ListView
import           Database                   (runDB)
import           Utils
import           Widgets.EntityList

data Overview a = Overview
  { overviewColumns            :: [ColumnDesc]                        -- table columns
  , overviewFilters            :: [EntityField a T.Text]              -- search filters
  , overviewDisplayRow         :: Entity a -> [String]                -- column values extraction function
  , overviewAddForm            :: Panel () -> IO (Maybe a)            -- add form
  , overviewEditForm           :: Maybe a -> Panel () -> IO (Maybe a) -- edit form
  , overviewIsItemDeletable    :: Maybe (Entity a -> IO Bool)         {- an optional action to determine
                                                                         whether it is safe to delete an item
                                                                      -}
  , overviewPostMutationAction :: Maybe (IO ())                       {- an optional action to execute after
                                                                         mutation of entities (add/edit/delete)
                                                                      -}
  , overviewFormSize           :: Size                                -- size of form
  }

overviewToEntityList :: Overview a -> EntityList a
overviewToEntityList o = EntityList (overviewColumns o) (overviewFilters o) (overviewDisplayRow o)

-- Creates an overview widget for a database table (a Persistent Entity).
-- Wraps around and entity list, extending it with the ability to add,
-- delete and edit rows.
overview :: (Labeled (EntityField a T.Text),
             PersistEntityBackend a ~ SqlBackend,
             PersistEntity a)
         => GuiContext
         -> Overview a
         -> IO Layout
overview ctx v = do
    let addForm = overviewAddForm v (guiPanel ctx)
    let editForm = flip (overviewEditForm v) (guiPanel ctx)
    let canDelete = fromMaybe (\_ -> return True) (overviewIsItemDeletable v)

    rowsVar <- variable []

    (lv, selectorBox, searchBox, entityListLayout) <- entityList ctx (overviewToEntityList v) rowsVar

    addButton    <- overviewAddButton ctx v addForm lv rowsVar selectorBox searchBox
    editButton   <- overviewEditButton ctx v editForm lv rowsVar selectorBox searchBox
                >>= enabledWhen lv (flip get selections >=> return . (== 1) . length)
    deleteButton <- overviewDeleteButton ctx v lv rowsVar selectorBox searchBox
                >>= enabledWhen lv (flip get selections >=>
                                    \sels -> do
                                        items <- listViewGetItems lv
                                        let selectedItems = mapMaybe (items `atMay`) sels
                                            hasSelections = not $ null sels
                                        allDeletable <- and <$> mapM canDelete selectedItems
                                        return $ hasSelections && allDeletable)

    return (margin 3 $
            row 2 [ vfill $ column 5 [ widget addButton
                                     , widget editButton
                                     , widget deleteButton]
                  , entityListLayout])

overviewAddButton :: (PersistEntityBackend a ~ SqlBackend,
                      PersistEntity a)
                  => GuiContext
                  -> Overview a
                  -> IO (Maybe a)
                  -> ListView (Entity a)
                  -> Var [Entity a]
                  -> ChoiceView (EntityField a T.Text) ()
                  -> ValueEntry T.Text ()
                  -> IO (Button ())
overviewAddButton ctx v form lv rowsVar selectorBox searchBox =
    button panel [text := "Dodaj",
                  on command := do mcolumn <- entityListActiveFilter elist selectorBox
                                   searchTerm <- entityListSearchTerm searchBox
                                   form >>= insertFromForm lv rowsVar mcolumn searchTerm
                                   fromMaybe (return ()) (overviewPostMutationAction v)]
    where panel = guiPanel ctx
          elist = overviewToEntityList v

overviewEditButton :: (PersistEntityBackend a ~ SqlBackend,
                      PersistEntity a)
                  => GuiContext
                  -> Overview a
                  -> (Maybe a -> IO (Maybe a))
                  -> ListView (Entity a)
                  -> Var [Entity a]
                  -> ChoiceView (EntityField a T.Text) ()
                  -> ValueEntry T.Text ()
                  -> IO (Button ())
overviewEditButton ctx v form lv rowsVar selectorBox searchBox =
    button panel [text := "Uredi",
                  enabled := False,
                  on command :=
                    do mval <- listViewGetSelectedVal lv
                       mcolumn <- entityListActiveFilter elist selectorBox
                       searchTerm <- entityListSearchTerm searchBox
                       case mval of
                           Just (Entity eId eVal) ->
                               do form (Just eVal) >>= editFromForm lv rowsVar eId mcolumn searchTerm
                                  fromMaybe (return ()) (overviewPostMutationAction v)
                           Nothing -> error "Error: Edit button enabled even though there was no selection."]
    where panel = guiPanel ctx
          elist = overviewToEntityList v

overviewDeleteButton :: (PersistEntityBackend a ~ SqlBackend,
                         PersistEntity a)
                     => GuiContext
                     -> Overview a
                     -> ListView (Entity a)
                     -> Var [Entity a]
                     -> ChoiceView (EntityField a T.Text) ()
                     -> ValueEntry T.Text ()
                     -> IO (Button ())
overviewDeleteButton ctx v lv rowsVar selectorBox searchBox =
    button panel [text := "Obriši",
                  enabled := False,
                  on command := do mcolumn <- entityListActiveFilter elist selectorBox
                                   searchTerm <- entityListSearchTerm searchBox
                                   yes <- proceedDialog mainFrame "Potvrdite" "Jeste li sigurni?"
                                   when yes $
                                       deleteSelections lv rowsVar mcolumn searchTerm
                                   fromMaybe (return ()) (overviewPostMutationAction v)]
    where mainFrame = guiFrame ctx
          panel = guiPanel ctx
          elist = overviewToEntityList v

insertFromForm :: (PersistEntityBackend a ~ SqlBackend,
                   PersistEntity a)
               => ListView (Entity a)
               -> Var [Entity a]
               -> Maybe (EntityField a T.Text)
               -> T.Text
               -> Maybe a
               -> IO ()
insertFromForm lv rowsVar mcolumn searchTerm mval =
    case mval of
      Just x -> do runDB $ insert_ x
                   updateEntityList lv rowsVar mcolumn searchTerm
      Nothing -> return ()

editFromForm :: (PersistEntityBackend a ~ SqlBackend,
                 PersistEntity a)
             => ListView (Entity a)
             -> Var [Entity a]
             -> Key a
             -> Maybe (EntityField a T.Text)
             -> T.Text
             -> Maybe a
             -> IO ()
editFromForm lv rowsVar eId mcolumn searchTerm mval = case mval of
    Just x -> do runDB $ replace eId x
                 updateEntityList lv rowsVar mcolumn searchTerm
    Nothing -> return ()

deleteSelections :: (PersistEntityBackend a ~ SqlBackend,
                     PersistEntity a)
                 => ListView (Entity a)
                 -> Var [Entity a]
                 -> Maybe (EntityField a T.Text)
                 -> T.Text
                 -> IO ()
deleteSelections lv rowsVar mcolumn searchTerm = do
    ixs <- get lv selections
    unless (null ixs) $ do
        currentRows <- listViewGetItems lv
        let rowsToDelete = mapMaybe (fmap entityKey . (currentRows `atMay`)) ixs
        runDB $ mapM_ delete rowsToDelete
        updateEntityList lv rowsVar mcolumn searchTerm

updateEntityList :: (PersistEntityBackend a ~ SqlBackend,
                     PersistEntity a)
                 => ListView (Entity a)
                 -> Var [Entity a]
                 -> Maybe (EntityField a T.Text)
                 -> T.Text
                 -> IO ()
updateEntityList lv rowsVar mcolumn searchTerm =
  do rows <- entityListFilteredRows mcolumn searchTerm
     listViewSetItems lv rows
     allRows <- entityListFilteredRows mcolumn ""
     set rowsVar [value := allRows]
