{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}

module Models (module Models,
               module M) where

import           Control.Monad.IO.Class
import           Control.Monad.Trans.Reader
import           Data.Decimal
import           Data.Text                  (Text)
import qualified Data.Text                  as T
import           Data.Time
import           Database.Persist
import           Database.Persist.Sql       (SqlBackend)
import           Database.Persist.TH        (mkMigrate, mkPersist,
                                             persistLowerCase, share,
                                             sqlSettings)

import           Classes
import           Models.Batch               as M
import           Models.Decimal             as M ()
import           Models.Department          as M
import           Models.Dose                as M
import           Models.DrugList            as M
import           Models.PricedBatch         as M
import           Models.Quantity            as M
import           Models.Transaction         as M

share [mkPersist sqlSettings, mkMigrate "migrateTables"] [persistLowerCase|
Doctor
    forename Text
    surname  Text
    deriving Show

Patient
    forename   Text
    surname    Text
    doctor     DoctorId
    doctorName Text
    deriving Show

Drug
    atkCode           Text
    genericName       Text
    proprietaryName   Text
    manufacturer      Text
    pricePerPackage   Decimal
    unitsPerPackage   Quantity
    unitDose          Dose
    typeDosePackaging Text
    list              DrugList
    UniqueDrug atkCode pricePerPackage
    deriving Show Eq Ord

Transaction
    type       TransactionType
    date       Day
    deriving Show

OutputTransactionDetails
    transaction TransactionId
    output      TransactionOutput
    patient     PatientId          Maybe
    department  Department         Maybe
    deriving Show

TransactionLine
    transaction  TransactionId
    drug         DrugId
    quantity     Quantity
    value        Decimal
    packagePrice Decimal
    deriving Show
|]

fetchDrug :: MonadIO m => Drug -> ReaderT SqlBackend m (Maybe (Entity Drug))
fetchDrug d = getBy $ UniqueDrug (drugAtkCode d) (drugPricePerPackage d)

instance Label (EntityField Doctor a) where
    labelled DoctorId       = ""
    labelled DoctorForename = "Ime"
    labelled DoctorSurname  = "Prezime"

instance Label (EntityField Patient a) where
    labelled PatientId         = ""
    labelled PatientForename   = "Ime"
    labelled PatientSurname    = "Prezime"
    labelled PatientDoctor     = "Liječnik"
    labelled PatientDoctorName = "Ime liječnika"

instance Label (EntityField Drug a) where
    labelled DrugId                = ""
    labelled DrugGenericName       = "Generičko ime"
    labelled DrugProprietaryName   = "Zaštićeno ime"
    labelled DrugAtkCode           = "ATK šifra"
    labelled DrugManufacturer      = "Proizvođač"
    labelled DrugPricePerPackage   = "Cijena pakiranja"
    labelled DrugTypeDosePackaging = "Opis"
    labelled DrugUnitsPerPackage   = "Jedinica u pakiranju"
    labelled DrugUnitDose          = "Jačina"
    labelled DrugList              = "Lista"

instance Label (Entity Doctor) where
    labelled d = name d ++ " " ++ surname d
        where name = T.unpack . doctorForename . entityVal
              surname = T.unpack . doctorSurname . entityVal
