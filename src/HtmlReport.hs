module HtmlReport where

import Control.Monad (forM_)
import Data.Monoid ((<>))
import qualified Data.Text                  as T
import Data.Time.Calendar

import Text.Blaze.Html5                     as H hiding (map)
import Text.Blaze.Html.Renderer.String      as R
import Text.Blaze.Html5.Attributes          as A hiding (title)

import           Classes
import           Models
import           Utils

tableCss :: Html
tableCss = "/*! Pure v0.6.0 Copyright 2014 Yahoo! Inc. All rights reserved.  Licensed under the BSD License.  https://github.com/yahoo/pure/blob/master/LICENSE.md */ .pure-table{border-collapse:collapse;border-spacing:0;empty-cells:show;border:1px solid #cbcbcb}.pure-table caption{color:#000;font:italic 85%/1 arial,sans-serif;padding:1em 0;text-align:center}.pure-table td,.pure-table th{border-left:1px solid #cbcbcb;border-width:0 0 0 1px;font-size:inherit;margin:0;overflow:visible;padding:.5em 1em}.pure-table td:first-child,.pure-table th:first-child{border-left-width:0}.pure-table thead{background-color:#e0e0e0;color:#000;text-align:left;vertical-align:bottom}.pure-table td{background-color:transparent}.pure-table-odd td{background-color:#f2f2f2}.pure-table-striped tr:nth-child(2n-1) td{background-color:#f2f2f2}.pure-table-bordered td{border-bottom:1px solid #cbcbcb}.pure-table-bordered tbody>tr:last-child>td{border-bottom-width:0}.pure-table-horizontal td,.pure-table-horizontal th{border-width:0 0 1px;border-bottom:1px solid #cbcbcb}.pure-table-horizontal tbody>tr:last-child>td{border-bottom-width:0}"

totalCss :: Html
totalCss = toHtml . unlines $
            [".total { float: right; }",
             ".table { width: inherit; }",
             "hr { color: #f2f2f2; }",
             "#content { width: 100%; }"]

css :: Html
css = tableCss <> totalCss

inputColumns :: [Html]
inputColumns =
    [ "Kom."
    , toHtml (labelled DrugAtkCode)
    , toHtml (labelled DrugPricePerPackage)
    , toHtml (labelled DrugGenericName)
    , toHtml (labelled DrugProprietaryName)
    , toHtml (labelled DrugTypeDosePackaging)
    , "Vrijednost"
    , "Datum"]

inputsToValues :: (Transaction, PricedBatch Drug) -> [Html]
inputsToValues t = fmap (toHtml . ($ t))
    [ show . pricedBatchQuantity . snd
    , labelled . drugAtkCode . pricedBatchItem . snd
    , T.unpack . price . drugPricePerPackage . pricedBatchItem . snd
    , labelled . drugGenericName . pricedBatchItem . snd
    , labelled . drugProprietaryName . pricedBatchItem . snd
    , labelled . drugTypeDosePackaging . pricedBatchItem . snd
    , T.unpack . price . pricedBatchPrice . snd
    , show . transactionDate . fst]

outputColumns :: [Html]
outputColumns =
    [ "Kom."
    , toHtml (labelled DrugAtkCode)
    , toHtml (labelled DrugPricePerPackage)
    , toHtml (labelled DrugGenericName)
    , toHtml (labelled DrugProprietaryName)
    , toHtml (labelled DrugTypeDosePackaging)
    , "Vrijednost"
    , "Izlaz na"
    , "Datum"]

outputsToValues :: ((Transaction, OutputSpecification), PricedBatch Drug) -> [Html]
outputsToValues t = map (toHtml . ($ t))
    [ show . pricedBatchQuantity . snd
    , labelled . drugAtkCode . pricedBatchItem . snd
    , T.unpack . price . drugPricePerPackage . pricedBatchItem . snd
    , labelled . drugGenericName . pricedBatchItem . snd
    , labelled . drugProprietaryName . pricedBatchItem . snd
    , labelled . drugTypeDosePackaging . pricedBatchItem . snd
    , T.unpack . price . pricedBatchPrice . snd
    , snd . fst
    , show . transactionDate . fst . fst]

stockColumns :: [Html]
stockColumns =
    [ "Kom."
    , toHtml (labelled DrugAtkCode)
    , toHtml (labelled DrugPricePerPackage)
    , toHtml (labelled DrugGenericName)
    , toHtml (labelled DrugProprietaryName)
    , toHtml (labelled DrugTypeDosePackaging)
    , "Vrijednost"]

stockToValues :: PricedBatch Drug -> [Html]
stockToValues b = map (toHtml . ($ b))
    [ show . pricedBatchQuantity
    , labelled . drugAtkCode . pricedBatchItem
    , T.unpack . price . drugPricePerPackage . pricedBatchItem
    , labelled . drugGenericName . pricedBatchItem
    , labelled . drugProprietaryName . pricedBatchItem
    , labelled . drugTypeDosePackaging . pricedBatchItem
    , T.unpack . price . pricedBatchPrice]

report :: Html -> Html -> Html
report title content =
    docTypeHtml $ do
        H.head $ do
            H.title title
            meta ! charset "utf-8"
            H.style css
        content

evenRow :: Html -> Html
evenRow = tr

oddRow :: Html -> Html
oddRow = tr ! class_ "pure-table-odd"

reportTable columns transactions batchToItems totalValue =
    H.div ! A.id "content" $ do
        table ! class_ "pure-table table" $ do
            thead . tr . mapM_ th $ columns
            tbody $ forM_ (zip [1..] transactions) (\(i, t) ->
                if even i
                    then evenRow . batchToItems $ t
                    else oddRow . batchToItems $ t)
        hr
        H.div ! class_ "total" $ ("Ukupno: " <> toHtml (totalValue transactions))

inputsHtmlReport :: Day -> Day -> [(Transaction, PricedBatch Drug)] -> String
inputsHtmlReport d1 d2 transactions = R.renderHtml $
    report "Ulazi" $
        body $ do
            h1 $ "Ulazi od " <> toHtml (showGregorian d1) <> " do " <> toHtml (showGregorian d2)
            reportTable inputColumns transactions batchToItems totalValue
    where totalValue = price . valueOfBatches . fmap snd
          batchToItems = mapM_ td . inputsToValues

outputsHtmlReport :: Day -> Day -> [((Transaction, OutputSpecification), PricedBatch Drug)] -> String
outputsHtmlReport d1 d2 transactions = R.renderHtml $
    report "Izlazi" $
        body $ do
            h1 $ "Izlazi od " <> toHtml (showGregorian d1) <> " do " <> toHtml (showGregorian d2)
            reportTable outputColumns transactions batchToItems totalValue
    where totalValue = price . valueOfBatches . fmap snd
          batchToItems = mapM_ td . outputsToValues

stockHtmlReport :: Day -> [PricedBatch Drug] -> String
stockHtmlReport date transactions = R.renderHtml $
    report "Stanje" $
        body $ do
            h1 $ "Stanje na " <> toHtml (showGregorian date)
            reportTable stockColumns transactions batchToItems totalValue
    where totalValue = price . valueOfBatches
          batchToItems = mapM_ td . stockToValues
