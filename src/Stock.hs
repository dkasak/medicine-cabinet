module Stock where

import           Control.Arrow
import           Control.Monad              (liftM, when)
import           Control.Monad.IO.Class
import           Control.Monad.Trans.Reader
import qualified Data.Function              as F
import           Data.List                  (groupBy)
import           Data.Maybe
import           Data.Time
import           Database.Esqueleto         hiding (groupBy, isNothing)
import qualified Database.Persist           as P
import           Graphics.UI.WX             (Var, varCreate, varGet, varSet)
import           Safe
import           System.IO.Unsafe           (unsafePerformIO)

import           Database
import           Models
import           Utils

{-# NOINLINE stock #-}
stock :: Var [PricedBatch Drug]
stock = unsafePerformIO $ varCreate []

setStock :: [PricedBatch Drug] -> IO ()
setStock = varSet stock

getStock :: IO [PricedBatch Drug]
getStock = varGet stock

transactionLinesOf :: MonadIO m => TransactionId -> ReaderT SqlBackend m [Entity TransactionLine]
transactionLinesOf tId =
    select $ from $ \tl -> do
    where_ (tl ^. TransactionLineTransaction ==. val tId)
    return tl

transactionsAndLinesAsOf :: MonadIO m
                         => Day
                         -> ReaderT SqlBackend m [(Entity Transaction, Entity TransactionLine)]
transactionsAndLinesAsOf date =
    select $ from $ \(tl `InnerJoin` t) -> do
             on (tl ^. TransactionLineTransaction ==. t ^. TransactionId)
             where_ (t ^. TransactionDate <=. val date
                 &&. t ^. TransactionType `in_` valList [Input, Output])
             orderBy [asc (t ^. TransactionDate), asc (t ^. TransactionId)]
             return (t, tl)

transactionsAndLinesSince :: MonadIO m
                          => Day
                          -> ReaderT SqlBackend m [(Entity Transaction, Entity TransactionLine)]
transactionsAndLinesSince date = do
    today <- liftIO getToday
    transactionsAndLinesSinceUntil date today

transactionsAndLinesSinceUntil
    :: MonadIO m
    => Day
    -> Day
    -> ReaderT SqlBackend m [(Entity Transaction, Entity TransactionLine)]
transactionsAndLinesSinceUntil date1 date2 =
    select $ from $ \(tl `InnerJoin` t) -> do
             on (tl ^. TransactionLineTransaction ==. t ^. TransactionId)
             where_ (t ^. TransactionDate >. val date1
                 &&. t ^. TransactionDate <=. val date2
                 &&. t ^. TransactionType `in_` valList [Input, Output])
             orderBy [asc (t ^. TransactionDate), asc (t ^. TransactionId)]
             return (t, tl)


transactionsAndLinesForAsOf :: MonadIO m
                            => [DrugId]
                            -> Day
                            -> ReaderT SqlBackend m [(Entity Transaction, Entity TransactionLine)]
transactionsAndLinesForAsOf drugIds date =
    select $ from $ \(d `InnerJoin` tl `InnerJoin` t) -> do
             on (tl ^. TransactionLineTransaction ==. t ^. TransactionId)
             on (tl ^. TransactionLineDrug ==. d ^. DrugId)
             where_ (t ^. TransactionDate <=. val date
                 &&. t ^. TransactionType `in_` valList [Input, Output]
                 &&. d ^. DrugId `in_` valList drugIds)
             orderBy [asc (t ^. TransactionDate), asc (t ^. TransactionId)]
             return (t, tl)

transactionsAndLinesForSince :: MonadIO m
                             => [DrugId]
                             -> Day
                             -> ReaderT SqlBackend m [(Entity Transaction, Entity TransactionLine)]
transactionsAndLinesForSince drugIds date = do
    today <- liftIO getToday
    transactionsAndLinesForSinceUntil drugIds date today

transactionsAndLinesForSinceUntil
    :: MonadIO m
    => [DrugId]
    -> Day
    -> Day
    -> ReaderT SqlBackend m [(Entity Transaction, Entity TransactionLine)]
transactionsAndLinesForSinceUntil drugIds date1 date2 =
    select $ from $ \(d `InnerJoin` tl `InnerJoin` t) -> do
             on (tl ^. TransactionLineTransaction ==. t ^. TransactionId)
             on (tl ^. TransactionLineDrug ==. d ^. DrugId)
             where_ (t ^. TransactionDate >. val date1
                 &&. t ^. TransactionDate <=. val date2
                 &&. t ^. TransactionType `in_` valList [Input, Output]
                 &&. d ^. DrugId `in_` valList drugIds)
             orderBy [asc (t ^. TransactionDate), asc (t ^. TransactionId)]
             return (t, tl)

batchesAsOf :: MonadIO m => Day -> ReaderT SqlBackend m [PricedBatch Drug]
batchesAsOf date = do
    lines <- transactionsAndLinesAsOf date
    transactionsAndLinesToBatches lines

batchesSince :: MonadIO m => Day -> ReaderT SqlBackend m [PricedBatch Drug]
batchesSince date = do
    lines <- transactionsAndLinesSince date
    transactionsAndLinesToBatches lines

batchesSinceUntil :: MonadIO m => Day -> Day -> ReaderT SqlBackend m [PricedBatch Drug]
batchesSinceUntil date1 date2 = do
    lines <- transactionsAndLinesSinceUntil date1 date2
    transactionsAndLinesToBatches lines

batchesForAsOf :: MonadIO m => [DrugId] -> Day -> ReaderT SqlBackend m [PricedBatch Drug]
batchesForAsOf drugIds date = do
    lines <- transactionsAndLinesForAsOf drugIds date
    transactionsAndLinesToBatches lines

batchesForSince :: MonadIO m => [DrugId] -> Day -> ReaderT SqlBackend m [PricedBatch Drug]
batchesForSince drugIds date = do
    lines <- transactionsAndLinesForSince drugIds date
    transactionsAndLinesToBatches lines

batchesForSinceUntil :: MonadIO m => [DrugId] -> Day -> Day -> ReaderT SqlBackend m [PricedBatch Drug]
batchesForSinceUntil drugIds date1 date2 = do
    lines <- transactionsAndLinesForSinceUntil drugIds date1 date2
    transactionsAndLinesToBatches lines

batchesByDateSince :: MonadIO m => Day -> ReaderT SqlBackend m [[PricedBatch Drug]]
batchesByDateSince date = do
    lines <- transactionsAndLinesSince date
    transactionsAndLinesToBatchesByDate lines

batchesByDateForSince :: MonadIO m => [DrugId] -> Day -> ReaderT SqlBackend m [[PricedBatch Drug]]
batchesByDateForSince drugIds date = do
    lines <- transactionsAndLinesForSince drugIds date
    transactionsAndLinesToBatchesByDate lines

transactionsAndLinesToBatchesByDate
    :: MonadIO m
    => [(Entity Transaction, Entity TransactionLine)]
    -> ReaderT SqlBackend m [[PricedBatch Drug]]
transactionsAndLinesToBatchesByDate ts =
    sequence $ mapM transactionAndLineToBatch <$> groups
    where groups = groupBy ((==) `F.on` dateOfTransaction) ts
          dateOfTransaction = transactionDate . entityVal . fst

transactionsAndLinesToBatches :: MonadIO m
                              => [(Entity Transaction, Entity TransactionLine)]
                              -> ReaderT SqlBackend m [PricedBatch Drug]
transactionsAndLinesToBatches = mapM transactionAndLineToBatch

transactionsAndLinesToUnsignedBatches :: MonadIO m
                              => [(Entity Transaction, Entity TransactionLine)]
                              -> ReaderT SqlBackend m [PricedBatch Drug]
transactionsAndLinesToUnsignedBatches = mapM transactionAndLineToUnsignedBatch

transactionAndLineToBatch :: MonadIO m
                          => (Entity Transaction, Entity TransactionLine)
                          -> ReaderT SqlBackend m (PricedBatch Drug)
transactionAndLineToBatch (eTransaction, eTransactionLine) =
    do batch <- transactionLineToBatch transactionLine
       return $ transaction `modifySign` batch
    where transactionLine = entityVal eTransactionLine
          transaction = entityVal eTransaction

transactionAndLineToUnsignedBatch :: MonadIO m
                          => (Entity Transaction, Entity TransactionLine)
                          -> ReaderT SqlBackend m (PricedBatch Drug)
transactionAndLineToUnsignedBatch (_, eTransactionLine) =
    transactionLineToBatch transactionLine
    where transactionLine = entityVal eTransactionLine


transactionLinesOfV :: MonadIO m => Key Transaction -> ReaderT SqlBackend m [TransactionLine]
transactionLinesOfV tId = map entityVal <$> getTransactionLines
    where getTransactionLines =
            select $
            from $ \tl -> do
            where_ (tl ^. TransactionLineTransaction ==. val tId)
            return tl

transactionDetailsOfV :: MonadIO m
                      => Key Transaction
                      -> ReaderT SqlBackend m (Maybe OutputTransactionDetails)
transactionDetailsOfV tId = fmap entityVal . headMay <$> getTransactionDetails
    where getTransactionDetails =
            select $
            from $ \o -> do
            where_ (o ^. OutputTransactionDetailsTransaction ==. val tId)
            return o

inputTransactionsAndLinesFromTo
    :: MonadIO m
    => Day
    -> Day
    -> ReaderT SqlBackend m [(Entity Transaction, Entity TransactionLine)]
inputTransactionsAndLinesFromTo date1 date2 =
    select $ from $ \(tl `InnerJoin` t) -> do
             on (tl ^. TransactionLineTransaction ==. t ^. TransactionId)
             where_ (t ^. TransactionDate >=. val date1
                 &&. t ^. TransactionDate <=. val date2
                 &&. t ^. TransactionType ==. val Input)
             orderBy [asc (t ^. TransactionDate), asc (t ^. TransactionId)]
             return (t, tl)

inputTransactionsAndBatchesFromTo
    :: Day
    -> Day
    -> IO [(Transaction, PricedBatch Drug)]
inputTransactionsAndBatchesFromTo date1 date2 = runDB $ do
    tl <- inputTransactionsAndLinesFromTo date1 date2
    batches <- transactionsAndLinesToBatches tl
    return $ zip (map (entityVal . fst) tl) batches

outputTransactionsAndLinesFromTo
    :: MonadIO m
    => Day
    -> Day
    -> ReaderT SqlBackend m [(Entity Transaction, Entity TransactionLine)]
outputTransactionsAndLinesFromTo date1 date2 =
    select $ from $ \(tl `InnerJoin` t) -> do
             on (tl ^. TransactionLineTransaction ==. t ^. TransactionId)
             where_ (t ^. TransactionDate >=. val date1
                 &&. t ^. TransactionDate <=. val date2
                 &&. t ^. TransactionType ==. val Output)
             orderBy [asc (t ^. TransactionDate), asc (t ^. TransactionId)]
             return (t, tl)

outputTransactionsAndBatchesFromTo
    :: Day
    -> Day
    -> IO [(Transaction, PricedBatch Drug)]
outputTransactionsAndBatchesFromTo date1 date2 = runDB $ do
    tl <- outputTransactionsAndLinesFromTo date1 date2
    batches <- transactionsAndLinesToUnsignedBatches tl
    return $ zip (map (entityVal . fst) tl) batches

outputTransactionsIdsAndBatchesFromTo
    :: Day
    -> Day
    -> IO [((Transaction, TransactionId), PricedBatch Drug)]
outputTransactionsIdsAndBatchesFromTo date1 date2 = runDB $ do
    tl <- outputTransactionsAndLinesFromTo date1 date2
    batches <- transactionsAndLinesToUnsignedBatches tl
    let transactionsAndIds = map ((entityVal . fst) &&& (entityKey . fst)) tl
    return $ zip transactionsAndIds batches

invalidateSummariesSince :: MonadIO m => Day -> ReaderT SqlBackend m ()
invalidateSummariesSince date =
    do tIds <- select $
               from $ \t -> do
               where_ (t ^. TransactionType ==. val Summary
                   &&. t ^. TransactionDate >=. val date)
               return $ t ^. TransactionId

       P.deleteWhere [TransactionId P.<-. map unValue tIds]
       P.deleteWhere [TransactionLineTransaction P.<-. map unValue tIds]

getSummaryOn :: MonadIO m => Day -> ReaderT SqlBackend m (Maybe (Entity Transaction))
getSummaryOn date =
    P.selectFirst [TransactionType P.==. Summary,
                   TransactionDate P.==. date]
                  []

getSummaryBefore :: MonadIO m => Day -> ReaderT SqlBackend m (Maybe (Entity Transaction))
getSummaryBefore date =
   P.selectFirst [TransactionType P.==. Summary,
                  TransactionDate P.<. date]
                 [P.Desc TransactionDate]

getLastSummary :: MonadIO m => ReaderT SqlBackend m (Maybe (Entity Transaction))
getLastSummary = getSummaryBefore =<< liftIO getToday

createSummaryTransaction :: IO ()
createSummaryTransaction = runDB $
    do today <- liftIO getToday
       let yesterday = (-1) `addDays` today

       summaryYesterday <- getSummaryOn yesterday

       when (isNothing summaryYesterday) $ do
           tid <- insert $ Transaction Summary yesterday
           stockYesterday <- liftIO $ recalculateStockOn yesterday
           let drugs = pricedBatchItem <$> stockYesterday
           mDrugIds <- sequence $ fetchDrug <$> drugs
           let drugIds = entityKey <$> catMaybes mDrugIds
               lines = toSummaryTransactionLine tid <$> zip drugIds stockYesterday
           mapM_ insertMany_ $ chunks 150 lines

toSummaryTransactionLine :: TransactionId -> (DrugId, PricedBatch Drug) -> TransactionLine
toSummaryTransactionLine tid (drugid, PricedBatch quantity drug value) =
    TransactionLine tid drugid (Quantity quantity) value packagePrice
    where packagePrice = drugPricePerPackage drug

recalculateStockFully :: IO [PricedBatch Drug]
recalculateStockFully = runDB $
    liftM joinPricedBatches (batchesAsOf =<< liftIO getToday)

recalculateStockOn :: Day -> IO [PricedBatch Drug]
recalculateStockOn date = runDB $
    do lastSummary <- getSummaryBefore date
       case lastSummary of
           Nothing -> liftM joinPricedBatches (batchesAsOf date)
           Just (Entity tId (Transaction _ summaryDate)) -> do
               summaryLines <- transactionLinesOf tId
               summaryBatches <- mapM (transactionLineToBatch . entityVal) summaryLines
               newBatches <- batchesSinceUntil summaryDate date
               return $ joinPricedBatches (newBatches ++ summaryBatches)

recalculateStock :: IO [PricedBatch Drug]
recalculateStock = recalculateStockOn =<< getToday

recalculateStockOnFor :: Day -> [DrugId] -> IO [PricedBatch Drug]
recalculateStockOnFor date drugIds = runDB $
    do lastSummary <- getSummaryBefore date
       case lastSummary of
           Nothing -> liftM joinPricedBatches (batchesForAsOf drugIds date)
           Just (Entity tId (Transaction _ summaryDate)) -> do
               -- TODO: Use transactionAndLineToBatch for this too (since we have both the lines and the transaction itself)
               summaryLines <- filter ((`elem` drugIds) . transactionLineDrug . entityVal) <$> transactionLinesOf tId
               summaryBatches <- mapM (transactionLineToBatch . entityVal) summaryLines
               newBatches <- batchesForSinceUntil drugIds summaryDate date
               return $ joinPricedBatches (newBatches ++ summaryBatches)

recalculateStockFor :: [DrugId] -> IO [PricedBatch Drug]
recalculateStockFor drugIds = flip recalculateStockOnFor drugIds =<< getToday

modifySign :: Transaction -> PricedBatch a -> PricedBatch a
modifySign (Transaction Input _)  = id
modifySign (Transaction Output _) = negatePricedBatch
modifySign (Transaction _ _)      = id

transactionLineToBatch :: MonadIO m => TransactionLine -> ReaderT SqlBackend m (PricedBatch Drug)
transactionLineToBatch (TransactionLine _ drugId quantity price _) =
  do mdrug <- P.get drugId
     let drug = fromMaybe
                    (error "Drug with requested ID is missing in the database.")
                    mdrug
     return $ PricedBatch (fromQuantity quantity) drug price
