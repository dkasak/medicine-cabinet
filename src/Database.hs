{-# LANGUAGE MultiParamTypeClasses #-}

module Database where

import           Control.Monad.IO.Class       (MonadIO)
import           Control.Monad.Logger         (NoLoggingT)
import           Control.Monad.Trans.Reader   (ReaderT)
import           Control.Monad.Trans.Resource (MonadBaseControl, ResourceT)
import qualified Data.Text                    as T
import           Database.Persist             hiding (get, update)
import           Database.Persist.Sql         (SqlPersistT)
import           Database.Persist.Sqlite      (runSqlite)

import           Config                       (databasePath)

selectAll :: (MonadIO m, PersistEntity val, PersistQuery backend, PersistEntityBackend val ~ BaseBackend backend) => ReaderT backend m [Entity val]
selectAll = selectList [] []

selectAllValues :: (MonadIO m, PersistEntity val, PersistQuery backend, PersistEntityBackend val ~ BaseBackend backend) => ReaderT backend m [val]
selectAllValues = fmap (map entityVal) selectAll

runDB :: (MonadBaseControl IO m, MonadIO m) => SqlPersistT (NoLoggingT (ResourceT m)) a -> m a
runDB = runSqlite (T.pack databasePath)

sqlLike :: PersistFilter
sqlLike = BackendSpecificFilter "like"

(=~.) :: EntityField record T.Text -> T.Text -> Filter record
col =~. term = Filter col term' sqlLike
    where term' = Left (T.concat [term, "%"])
