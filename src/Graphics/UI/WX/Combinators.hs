module Graphics.UI.WX.Combinators where

import Control.Monad      (forM_, void)
import Data.Monoid        (All (..))
import Graphics.UI.WX
import Graphics.UI.WXCore

import Classes

enabledWhen :: (Reactable a, Able (Window b)) => a -> (a -> IO Bool) -> Window b -> IO (Window b)
enabledWhen s pred w = react toggleWidget s >> return w
    where toggleWidget = pred s >>= (\state -> set w [enabled := state])

visibleWhen :: (Reactable a, Visible (Window b)) => a -> (a -> IO Bool) -> Window b -> IO (Window b)
visibleWhen s pred w = react toggleWidget s >> return w
    where toggleWidget = pred s
                     >>= \state -> set w [visible := state]
                                >> (windowReLayout =<< get w parent)

visibleWhenAll :: (Reactable a, Visible (Window b)) => a -> [a -> IO Bool] -> Window b -> IO (Window b)
visibleWhenAll s preds w = react toggleWidget s >> return w
    where toggleWidget = (and <$> mapM ($ s) preds)
                     >>= \state -> set w [visible := state]
                                >> (windowReLayout =<< get w parent)

enabledWhenMany :: (Reactable a, Able b) => [a] -> (a -> IO Bool) -> b -> IO b
enabledWhenMany ws pred w = forM_ ws (react toggleWidget) >> return w
    where widgetState ws = do
            rs <- mapM pred ws
            let state = getAll . mconcat . map All $ rs
            return state
          toggleWidget = widgetState ws >>= (\state -> set w [enabled := state])

setUpdateHandler :: Updating a => IO () -> a -> IO ()
setUpdateHandler h w = set w [on update :~ (>> h)]

setSelectHandler :: Selecting a => IO () -> a -> IO ()
setSelectHandler h w = set w [on select :~ (>> h)]

sized :: (Reactive b, Dimensions b) => Size -> b -> IO b
sized s w = set w [clientSize := s, on resize := set w [clientSize := s]]
         >> return w

showHide :: Window a -> [Window b] -> IO ()
showHide showing hidden =
    do void $ windowShow showing
       mapM_ windowHide hidden

onDateChange :: IO () -> EventCalendar -> IO ()
onDateChange action (CalendarDayChanged _) = action
onDateChange _      _                      = propagateEvent
