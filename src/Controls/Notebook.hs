module Controls.Notebook where

import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.Storable
import Graphics.UI.WXCore    hiding (Vector)
import System.IO.Unsafe

clickedPage :: Notebook () -> Point -> IO Int
clickedPage nb p = propagateEvent >> notebookHitTest nb p flag

{-# NOINLINE flag #-}
flag :: Ptr CInt
flag = unsafePerformIO flag'
  where flag' =
          do work <- malloc :: IO (Ptr CInt)
             poke work (fromIntegral wxBK_HITTEST_ONPAGE)
             return work
