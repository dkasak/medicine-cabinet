module Controls.ListView where

import Control.Monad
import Data.List
import Data.Maybe
import Graphics.UI.WX
import Graphics.UI.WX.Classes     ()
import Graphics.UI.WX.Events      ()
import Graphics.UI.WXCore
import Safe

import Classes
import Controls.ListCtrl          ()
import Graphics.UI.WX.Combinators

instance Reactable (ListView a) where
    react = setSelectHandler

instance Selections (ListView a) where
    selections = newAttr "selections" getter setter
        where getter w = get (listViewCtrl w) selections
              setter w ixs = set (listViewCtrl w) [selections := ixs]

instance Selecting (ListView a) where
    select = newEvent "select" getter setter
        where getter w = get (listViewCtrl w) (on select)
              setter w action = set (listViewCtrl w) [on select := action]

listViewGetSelectedVal :: ListView a -> IO (Maybe a)
listViewGetSelectedVal lv = do
    ixs <- get lv selections
    items <- listViewGetItems lv
    let item = join $ liftM (items `atMay`) (headMay ixs)
    return item

listViewHasSelection :: ListView a -> IO Bool
listViewHasSelection = fmap isJust . listViewGetSelectedVal

listViewHasSingleSelection :: ListView a -> IO Bool
listViewHasSingleSelection lv = liftM ((== 1) . length) (listViewGetSelectedVals lv)

listViewGetSelectedVals :: ListView a -> IO [a]
listViewGetSelectedVals lv = do
    ixs <- get lv selections
    items <- listViewGetItems lv
    return $ mapMaybe (items `atMay`) ixs

listViewSetItemsKeepSelection :: ListView a -> [a] -> IO ()
listViewSetItemsKeepSelection lv xs =
    do ixs <- get lv selections
       listViewSetItems lv xs
       let lc = listViewCtrl lv
       void $ fromMaybe (return True) (focusOnElem lc <$> headMay ixs)
       set lv [selections := ixs]
    where focusOnElem lc x = listCtrlSetItemState lc x wxLIST_STATE_FOCUSED wxLIST_STATE_FOCUSED

listViewDeleteSelectedItems :: Eq a => ListView a -> IO ()
listViewDeleteSelectedItems lv =
    do selected <- listViewGetSelectedVals lv
       all <- listViewGetItems lv
       listViewSetItems lv (all \\ selected)

