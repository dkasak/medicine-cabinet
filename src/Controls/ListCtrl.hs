module Controls.ListCtrl where

import Data.Bits          ((.|.))
import Graphics.UI.WX
import Graphics.UI.WXCore
-- import Graphics.UI.WX.Classes ()
-- import Graphics.UI.WX.Events ()
--
-- import Classes ()
import Utils              (iterateUntilM')

-- It seems that for ListCtrls it's only possible to register handlers
-- that accept an EventList parameter which further specifies which kind of
-- event happened. This also extends to ListView which wraps ListCtrl.
--
-- This means it's not as easy to handle single events for ListCtrl which
-- is probably why the Selecting instance wasn't implemented for it in the
-- first place. Nevertheless, we can fake up a good enough instance by
-- returning an empty action when the registered select handler is
-- requested. For setting the select handler, we only overload the
-- ListItemSelected and ListItemDeselected events and pass all other events
-- to the previously registered handler. Care should be taken that a new
-- handler is not registered too often in order not to build the function
-- chain too long.
-- We also need to override events for adding and deleting items since when
-- those happen, items can get deselected without a ListItemDeselected
-- event firing.
instance Selecting (ListCtrl a) where
    select = newEvent "select" getter setter
        where getter _ = return (return ())
              setter l action = do oldHandler <- listCtrlGetOnListEvent l
                                   let eventHandler e@(ListItemSelected _) = oldHandler e >> action
                                       eventHandler e@(ListItemDeselected _) = oldHandler e >> action
                                       eventHandler e@(ListInsertItem _) = oldHandler e >> action
                                       eventHandler e@(ListDeleteItem _) = oldHandler e >> action
                                       eventHandler e@ListDeleteAllItems = oldHandler e >> action
                                       eventHandler e = oldHandler e
                                   listCtrlOnListEvent l eventHandler

instance Selections (ListCtrl a) where
    selections = newAttr "selections" getter setter
        where getter = selectedItems
              setter w ixs =
                do oldIxs <- selectedItems w :: IO [Int]
                   sequence_ [deselectItem w i | i <- oldIxs, i `notElem` ixs]
                   mapM_ (selectItem w) ixs
              nextSelectedItem w i = listCtrlGetNextItem w i wxLIST_NEXT_ALL wxLIST_STATE_SELECTED
              selectedItems w = iterateUntilM' (< 0) (nextSelectedItem w) (-1)
              selectItem w i = listCtrlSetItemState w i wxLIST_STATE_SELECTED wxLIST_STATE_SELECTED
              deselectItem w i = listCtrlSetItemState w i 0 (wxLIST_STATE_SELECTED .|. wxLIST_STATE_FOCUSED)

