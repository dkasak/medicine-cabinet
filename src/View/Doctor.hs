module View.Doctor where

import Control.Applicative (ZipList (..))
import Data.Maybe
import Database.Persist    (Entity, entityVal)
import Graphics.UI.WX
import Graphics.UI.XTC     (typedValue)

import Classes
import Models
import Utils
import Widgets.Form        (FormField (..), form, formAcceptButton,
                            formCloseButton, setFormLayout, showForm, textField)
import Widgets.Overview

doctorOverview :: Overview Doctor
doctorOverview = Overview
  { overviewColumns = [ColumnDesc (labelled DoctorForename)
                                  Nothing
                                  (Just 150),
                       ColumnDesc (labelled DoctorSurname)
                                  Nothing
                                  (Just 150)]
  , overviewFilters = [DoctorForename, DoctorSurname]
  , overviewDisplayRow = doctorValues
  , overviewAddForm = doctorForm Nothing
  , overviewEditForm = doctorForm
  , overviewIsItemDeletable = Nothing
  , overviewPostMutationAction = Nothing
  , overviewFormSize = sz 300 (-1)
  }

doctorValues :: Entity Doctor -> [String]
doctorValues ed = map (labelled . ($ doctor)) [doctorForename, doctorSurname]
    where doctor = entityVal ed

doctorForm :: Maybe Doctor -> Panel () -> IO (Maybe Doctor)
doctorForm doctor frame = do
    form'     <- form frame
    fForename <- textField form'
    fSurname  <- textField form'

    maybe (return ()) (\d ->
        do set fForename [typedValue := Just $ doctorForename d]
           set fSurname [typedValue := Just $ doctorSurname d]) doctor

    let fields = getZipList
               $ ZipList [ValueField fForename, ValueField fSurname]
             <*> ZipList (map columnName . overviewColumns $ doctorOverview)

    submit <- formAcceptButton (isJust doctor) form' fields
    close  <- formCloseButton form'

    let size = overviewFormSize doctorOverview
    setFormLayout "Unos doktora" form' fields size submit close

    showForm form' submit close >>= \ret ->
        case ret of
            Just _ -> do forename <- get fForename typedValue
                         surname  <- get fSurname typedValue
                         return $ Doctor <$> forename <*> surname
            Nothing -> return Nothing

doctorView :: GuiContext -> IO Layout
doctorView ctx = overview ctx doctorOverview
