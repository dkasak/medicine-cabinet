module View.Patient where

import           Control.Applicative (ZipList (..))
import           Data.Maybe
import qualified Data.Text           as T
import           Database.Persist    (Entity (..), entityKey, entityVal)
import qualified Database.Persist    as P (get)
import           Graphics.UI.WX
import           Graphics.UI.XTC

import           Classes
import           Database            (runDB, selectAll)
import           Models
import           Utils
import           Widgets.Form        (FormField (..), form, formAcceptButton,
                                      formCloseButton, setFormLayout, showForm,
                                      textField)
import           Widgets.Overview

patientOverview :: Overview Patient
patientOverview = Overview
  { overviewColumns = [ColumnDesc (labelled PatientForename)
                                  Nothing
                                  (Just 100),
                       ColumnDesc (labelled PatientSurname)
                                  Nothing
                                  (Just 140),
                       ColumnDesc (labelled PatientDoctor)
                                  Nothing
                                  (Just 140)]
  , overviewFilters = [PatientForename, PatientSurname]
  , overviewDisplayRow = patientValues
  , overviewAddForm = patientForm Nothing
  , overviewEditForm = patientForm
  , overviewIsItemDeletable = Nothing
  , overviewPostMutationAction = Nothing
  , overviewFormSize = sz 300 (-1)
  }

patientValues :: Entity Patient -> [String]
patientValues ep = map (labelled . ($ patient))
    [ patientForename
    , patientSurname
    , patientDoctorName]
    where patient = entityVal ep

patientForm :: Maybe Patient -> Panel () -> IO (Maybe Patient)
patientForm patient frame = do
    form'     <- form frame
    fForename <- textField form'
    fSurname  <- textField form'
    fDoctor   <- mkChoiceView form' []

    doctors <- runDB selectAll
    set fDoctor [typedItems := doctors]

    maybe (return ()) (\p ->
        do set fForename [typedValue := Just $ patientForename p]
           set fSurname [typedValue := Just $ patientSurname p]
           let doctorKey = patientDoctor p
           doctorVal <- fromJust <$> runDB (P.get (patientDoctor p))
           set fDoctor [typedMaybeSelection := Just (Entity doctorKey doctorVal)]) patient

    let fields = getZipList
               $ ZipList [ValueField fForename,
                          ValueField fSurname,
                          ChoiceField fDoctor]
             <*> ZipList (map columnName . overviewColumns $ patientOverview)

    submit <- formAcceptButton (isJust patient) form' fields
    close  <- formCloseButton form'

    let size = overviewFormSize patientOverview
    setFormLayout "Unos pacijenta" form' fields size submit close

    showForm form' submit close >>= \ret ->
        case ret of
            Just _ -> do forename   <- get fForename typedValue
                         surname    <- get fSurname typedValue
                         doctor     <- fmap entityKey <$> get fDoctor typedMaybeSelection
                         doctorName <- fmap (formatDoctor . entityVal) <$> get fDoctor typedMaybeSelection
                         return $ Patient <$> forename <*> surname <*> doctor <*> doctorName
            Nothing -> return Nothing

formatDoctor :: Doctor -> T.Text
formatDoctor = (\name surname -> T.concat [name, " ", surname]) <$> doctorForename <*> doctorSurname

patientView :: GuiContext -> IO Layout
patientView ctx = overview ctx patientOverview
