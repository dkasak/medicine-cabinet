module View.Inventory where

import           Control.Monad
import qualified Data.Text                  as T
import           Graphics.UI.WX             hiding (sorted, when)
import           Graphics.UI.WXCore         (windowFreeze, windowThaw)
import           Graphics.UI.XTC            hiding (ListView)

import           Classes
import           Graphics.UI.WX.Combinators
import           Stock
import           Utils
import           View.Inventory.Input
import           View.Inventory.Output
import           View.Inventory.State
import           View.TransactionHistory
import           Widgets.TypedList

data InventoryMode = StockMode | InputMode | OutputMode
    deriving (Show, Eq, Enum, Bounded)

instance Label InventoryMode where
    labelled StockMode  = "Stanje"
    labelled InputMode  = "Ulaz"
    labelled OutputMode = "Izlaz"

switchScreens :: GuiContext
              -> ChoiceView InventoryMode ()
              -> (Panel (), Panel (), Panel ()) -> IO ()
switchScreens ctx modeSelector (stockPage, inputPage, outputPage) =
    set modeSelector [on select :=
        do mode <- get modeSelector typedMaybeSelection
           void $ case mode of
               Just StockMode -> do
                   windowFreeze frame
                   showHide stockPage [inputPage, outputPage]
                   windowReFit inventoryTab
                   windowThaw frame
               Just InputMode -> do
                   windowFreeze frame
                   showHide inputPage [stockPage, outputPage]
                   windowReFit inventoryTab
                   windowThaw frame
               Just OutputMode -> do
                   windowFreeze frame
                   showHide outputPage [stockPage, inputPage]
                   windowReFit inventoryTab
                   windowThaw frame
               Nothing -> return ()]
    where inventoryTab = guiPanel ctx
          frame = guiFrame ctx

inventoryView :: GuiContext -> HistoryView -> IO (IO (), Layout)
inventoryView ctx history = do
    let inventoryTab = guiPanel ctx

    modeSelector <- mkChoiceView inventoryTab [typedItems := [(minBound :: InventoryMode) ..],
                                               typedMaybeSelection := Just minBound]
    stockPage    <- panel inventoryTab []
    inputPage    <- panel inventoryTab []
    outputPage   <- panel inventoryTab []

    showHide stockPage [outputPage, inputPage]

    switchScreens ctx modeSelector (stockPage, inputPage, outputPage)

    stockList <- typedList stockPage stockTypedListMeta
    stock <- getStock
    stockValue <- staticText stockPage [text := T.unpack . price $ valueOfBatches stock]
    let stockLayout = column 2 [typedListLayout stockList,
                                hstretch $ row 2 [label "Ukupno: ", widget stockValue]]

    (updateInventoryOutputView, outputStockList, outputLayout) <- inventoryOutput ctx { guiPanel = outputPage } stockList history
    inputLayout <- inventoryInput ctx { guiPanel = inputPage } stockList outputStockList history

    let inventoryLayout =
           container inventoryTab $
           margin 3 $
           fill $ column 5 [row 2 [hspace 4,
                                   center $ label "Pregled: ",
                                   marginTop . margin 2 . hfill $ widget modeSelector],
                            fill $ container stockPage stockLayout,
                            fill $ container inputPage inputLayout,
                            fill $ container outputPage outputLayout]

    -- Returns an "update the stock lists" action, along with the layout
    return (getStock >>= \stock ->
              do mapM_ (flip listViewSetItems stock . typedListView) [stockList, outputStockList]
                 set stockValue [text := T.unpack . price $ valueOfBatches stock]
                 updateInventoryOutputView,
            inventoryLayout)
