module View.Inventory.Misc where

import qualified Data.Function        as F (on)
import           Data.MonoTraversable
import qualified Data.Text            as T
import           Graphics.UI.WX       hiding (sorted, when)

import           Classes
import           Controls.ListView
import           Models
import           Utils
import           Widgets.EntityList

updateSelectedQuantity :: Eq a => (Integer -> Integer) -> ListView (Batch a) -> IO ()
updateSelectedQuantity f transactionList =
    do selected <- fmap (omap (ifResult (> 0) f)) <$> listViewGetSelectedVals transactionList
       all <- listViewGetItems transactionList
       listViewSetItemsKeepSelection transactionList (zigzag ((==) `F.on` batchItem) all selected)

batchEntityList :: EntityList (Batch Drug)
batchEntityList = EntityList
  { entityListColumns = [ColumnDesc "Kom."
                                    (Just AlignCentre)
                                    (Just 60),
                         ColumnDesc (labelled DrugAtkCode)
                                    Nothing
                                    (Just 95),
                         ColumnDesc "Cijena"
                                    Nothing
                                    (Just 75),
                         ColumnDesc (labelled DrugGenericName)
                                    Nothing
                                    (Just 120),
                         ColumnDesc (labelled DrugProprietaryName)
                                    Nothing
                                    (Just 120),
                         ColumnDesc (labelled DrugTypeDosePackaging)
                                    Nothing
                                    (Just 250)]
  , entityListFilters = []
  , entityListDisplayRow = const []
  }

batchValues :: Batch Drug -> [String]
batchValues b = map ($ b)
    [ show . batchQuantity
    , labelled . drugAtkCode . batchItem
    , T.unpack . price . drugPricePerPackage . batchItem
    , labelled . drugGenericName . batchItem
    , labelled . drugProprietaryName . batchItem
    , labelled . drugTypeDosePackaging . batchItem]

