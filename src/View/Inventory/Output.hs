{-# LANGUAGE MultiParamTypeClasses #-}

module View.Inventory.Output where

import           Control.Monad
import           Control.Monad.IO.Class
import           Control.Monad.Trans.Control (MonadBaseControl)
import           Data.Decimal
import           Data.List                   hiding (insert)
import           Data.List.NonEmpty          (NonEmpty (..), (<|))
import           Data.Maybe
import           Data.Ord                    (comparing)
import qualified Data.Text                   as T
import           Data.Time
import           Database.Esqueleto          hiding (get, on, select, set, (*.))
import           Graphics.UI.WX              hiding (sorted, when)
import           Graphics.UI.WXCore          hiding (when)
import           Graphics.UI.XTC             hiding (ListView)

import           Classes
import           Controls
import           Controls.ListView
import           Database
import           Models
import           Stock
import           Utils
import           View.Inventory.Misc
import           View.Patient                (patientOverview)
import           View.TransactionHistory
import           Widgets.EntityList
import           Widgets.Overview            (overviewToEntityList)
import           Widgets.ReportError
import           Widgets.TypedList

data OutputInfo = OutputInfo TransactionOutput (Maybe PatientId) (Maybe Department)

toOutputTransactionDetails :: TransactionId -> OutputInfo -> OutputTransactionDetails
toOutputTransactionDetails tid (OutputInfo output mpatient mdepartment) =
    OutputTransactionDetails tid output mpatient mdepartment

stockTypedListShortMeta :: TypedListMeta (PricedBatch Drug)
stockTypedListShortMeta = TypedListMeta
  {  typedListColumns = [ColumnDesc "Kom."
                                    (Just AlignCentre)
                                    (Just 60),
                         ColumnDesc (labelled DrugAtkCode)
                                    Nothing
                                    (Just 95),
                         ColumnDesc "Cijena"
                                    Nothing
                                    (Just 75),
                         ColumnDesc (labelled DrugGenericName)
                                    Nothing
                                    (Just 120),
                         ColumnDesc (labelled DrugProprietaryName)
                                    Nothing
                                    (Just 120),
                         ColumnDesc (labelled DrugTypeDosePackaging)
                                    Nothing
                                    (Just 140)]
  , typedListFilters = TypedListFilter (T.pack $ labelled DrugGenericName)
                                        (\t -> T.isPrefixOf t . drugGenericName . pricedBatchItem)
                                        (comparing (drugGenericName . pricedBatchItem))
                                        Ascending <|
                       TypedListFilter (T.pack $ labelled DrugProprietaryName)
                                        (\t -> T.isPrefixOf t . drugProprietaryName . pricedBatchItem)
                                        (comparing (drugProprietaryName . pricedBatchItem))
                                        Ascending <|
                       TypedListFilter (T.pack $ labelled DrugAtkCode)
                                        (\t -> T.isPrefixOf t . drugAtkCode . pricedBatchItem)
                                        (comparing (drugAtkCode . pricedBatchItem))
                                        Ascending :| []
  , typedListDisplayRow = stockValuesShort
  , typedListFetchValues = getStock
  }

stockValuesShort :: PricedBatch Drug -> [String]
stockValuesShort b = map ($ b)
    [ show . pricedBatchQuantity
    , labelled . drugAtkCode . pricedBatchItem
    , T.unpack . price . drugPricePerPackage . pricedBatchItem
    , labelled . drugGenericName . pricedBatchItem
    , labelled . drugProprietaryName . pricedBatchItem
    , labelled . drugTypeDosePackaging . pricedBatchItem]

scheduleSelectedForOutput
    :: (Ord a, TypedValued Integer w)
    => w
    -> ListView (PricedBatch a)
    -> ListView (Batch a)
    -> IO ()
scheduleSelectedForOutput quantityEntry itemList transactionList =
    do quantity <- fromMaybe 1 <$> get quantityEntry typedValue
       set quantityEntry [typedValue := Just 1]
       itemsToSchedule <- map toBatch <$> listViewGetSelectedVals itemList
       scheduledItems  <- listViewGetItems transactionList
       let itemsToSchedule' = tryToSchedule quantity <$> itemsToSchedule
           updatedDrugs     = joinBatches $ itemsToSchedule' ++ scheduledItems
       listViewSetItems transactionList updatedDrugs
    where tryToSchedule n b@(Batch availableQuantity item)
              | n > availableQuantity = b
              | otherwise             = Batch n item

submitOutputTransaction :: (MonadIO m, MonadBaseControl IO m)
                        => OutputInfo
                        -> Day
                        -> [Batch Drug]
                        -> m ()
submitOutputTransaction outputInfo day drugBatches' = runDB $
    do tid <- insert $ Transaction Output day

       let drugBatches = sortOn (drugAtkCode . batchItem) drugBatches'
           drugs = batchItem <$> drugBatches
       mDrugIds <- sequence $ fetchDrug <$> drugs
       let drugIds = entityKey <$> catMaybes mDrugIds
       stock <- liftIO $ sortOn (drugAtkCode . pricedBatchItem) <$> recalculateStockFor drugIds
       let lines = toOutputTransactionLine tid <$> zip3 drugIds stock drugBatches

       -- All summaries since the date of the transaction are now invalid.
       invalidateSummariesSince day

       mapM_ insertMany_ $ chunks 150 lines
       insert_ $ toOutputTransactionDetails tid outputInfo

doSubmitOutput :: (TypedSelection Department department,
                   TypedMaybeSelection TransactionOutput output)
                  => ListView (Batch Drug)
                  -> TypedList (PricedBatch Drug)
                  -> TypedList (PricedBatch Drug)
                  -> output
                  -> ListView (Entity Patient)
                  -> department
                  -> CalendarCtrl ()
                  -> HistoryView
                  -> IO ()
doSubmitOutput transactionList outputStockList stockList
               outputSelector patientList departmentSelector calendar history =
    do items      <- listViewGetItems transactionList
       mOutputTo  <- get outputSelector typedMaybeSelection
       mpatient   <- listViewGetSelectedVal patientList
       department <- get departmentSelector typedSelection
       frame <- get calendar rootParent

       let outputToPatient :: Bool
           outputToPatient = maybe False (ToPatient ==) mOutputTo

           patientSpecified :: Bool
           patientSpecified = isJust mpatient

           outputSpecified :: Bool
           outputSpecified = isJust mOutputTo

       unless (null items || not outputSpecified || (outputToPatient && not patientSpecified)) $ do
           day <- get calendar date

           -- Determine selected output information from GUI
           let output = fromJust mOutputTo
               (mpatient', mdepartment) =
                   case output of
                       ToPatient    -> (mpatient, Nothing)
                       ToDepartment -> (Nothing, Just department)
                       ToTrash      -> (Nothing, Nothing)
               outputInfo = OutputInfo output (entityKey <$> mpatient') mdepartment

           -- Check that the requested stock was/is available on the date of
           -- the transaction is to be submitted.
           -- Also check that the stock in question was not already spent after
           -- that date.
           let batchesArePositive = all ((>= 0) . batchQuantity)
               r `availableIn` q = batchesArePositive (joinBatchGroups (map negateBatch r) q)

           mDrugEntities <- runDB $ mapM (fetchDrug . batchItem) items
           let drugIds = map entityKey . catMaybes $ mDrugEntities

           stockOnRequestedDate <- toBatches <$> recalculateStockOnFor day drugIds
           batchesSinceRequestedDate <- runDB $ map toBatches <$> batchesByDateForSince drugIds day
           let stockAfterSpending = stockOnRequestedDate `joinBatchGroups` map negateBatch items
               stocksSinceDate = scanl' joinBatchGroups stockAfterSpending batchesSinceRequestedDate
               stockWasAvailable = items `availableIn` stockOnRequestedDate
               stockNotAlreadySpent = all batchesArePositive stocksSinceDate

           when (length drugIds /= length mDrugEntities) $
               reportError frame "Failed fetching some drugs in doSubmitOutput while checking whether transaction is valid."

           if stockWasAvailable && stockNotAlreadySpent
              then do submitOutputTransaction outputInfo day items
                      -- recalculate the stock and store it in a variable to avoid recalculation
                      liftIO $ recalculateStock >>= setStock

                      -- Update views
                      filterTypedList stockList
                      filterTypedList outputStockList
                      updateHistory history

                      -- Reset controls
                      -- The calendar line is such because of a bug in WXHaskell.
                      listViewSetItems transactionList []
                      getToday >>= fixedDayToWXDate >>= \d -> set calendar [date := d]
              else case (not stockWasAvailable, not stockNotAlreadySpent) of
                       (True, _)     -> errorDialog frame "Greška" $
                                         "Transakciju nije moguće izvršiti na traženi datum.\n\n" ++
                                         "Nekih od lijekova u transakciji tad nije bilo u zalihi."
                       (False, True) -> errorDialog frame "Greška" $
                                         "Transakciju nije moguće izvršiti na traženi datum.\n\n" ++
                                         "Neki od lijekova u transakciji već su potrošeni na kasniji datum."
                       _ -> return ()

toOutputTransactionLine :: TransactionId -> (DrugId, PricedBatch Drug, Batch Drug) -> TransactionLine
toOutputTransactionLine tid (drugid, PricedBatch totalUnits _ totalValue, Batch quantity drug) = line
    where line = TransactionLine tid drugid (Quantity unitsToAllocate) batchValue packagePrice
          -- FIXME: Check that the drugs in the second and third tuple elements are the same drugs.
          --        If they're not, something is wrong, so don't generate a transaction line.
          -- FIXME: What if unitsToAllocate ends up being <= 0?
          -- Both problems above can be solved by making toOutputTransactionLine return
          -- a Maybe TransactionLine.
          unitsToAllocate = min totalUnits quantity
          (batchValue:_) = totalValue `allocate` [unitsToAllocate, totalUnits - unitsToAllocate]
          packagePrice = drugPricePerPackage drug

inventoryOutput :: GuiContext
                -> TypedList (PricedBatch Drug)
                -> HistoryView
                -> IO (IO (), TypedList (PricedBatch Drug), Layout)
inventoryOutput ctx stockList history = do
    let pane = guiPanel ctx

    outputStockList <- typedList pane stockTypedListShortMeta
    transactionList <- listView' pane (entityListColumns batchEntityList) batchValues

    let drugStockList = typedListView outputStockList
        drugStockLayout = typedListLayout outputStockList

    quantityEntry <- mkValueEntry pane [typedValue := Just 1, alignment := AlignCentre]
    scheduleButton <- button pane
        [text := "→",
         tooltip := "Dodaj u transakciju",
         on command := scheduleSelectedForOutput quantityEntry drugStockList transactionList]
    plusButton <- button' (sz 40 30) pane
        [text := "+",
         tooltip := "Povećaj za 1",
         on command := updateSelectedQuantity (+1) transactionList]
    minusButton <- button' (sz 40 30) pane
        [text := "-",
         tooltip := "Umanji za 1",
         on command := updateSelectedQuantity (+ negate 1) transactionList]
    deleteButton <- button' (sz 40 30) pane
        [text := "×",
         tooltip := "Obriši",
         on command := listViewDeleteSelectedItems transactionList]

    calendar <- calendarCtrl pane []

    outputTo <- mkChoiceView pane [typedItems := [(minBound :: TransactionOutput) ..],
                                   typedMaybeSelection := Just minBound]

    toPatient    <- panel pane []
    toDepartment <- panel pane []

    windowShow toPatient
    windowHide toDepartment

    department <- mkRadioView toDepartment Horizontal [minBound..maxBound]
                         [typedSelection := (minBound :: Department)]
    patientsVar <- variable []
    (patient, patientFilter, patientSearch, patientLayout) <- entityList
                                                                ctx { guiPanel = toPatient }
                                                                (overviewToEntityList patientOverview)
                                                                patientsVar

    submitButton <- button pane [text := "Pohrani",
                                 on command :=
                                    doSubmitOutput transactionList outputStockList stockList
                                                   outputTo patient department calendar history]

    set outputTo [on select :=
        do mode <- get outputTo typedMaybeSelection
           void $ case mode of
               Just ToPatient -> do
                   windowHide toDepartment
                   windowShow toPatient
                   windowReLayout pane
                   windowReLayout =<< get toPatient parent
               Just ToDepartment -> do
                   windowHide toPatient
                   windowShow toDepartment
                   windowReLayout pane
                   windowReLayout =<< get toDepartment parent
               Just ToTrash -> do
                   windowHide toPatient
                   windowHide toDepartment
                   windowReLayout pane
                   windowReLayout =<< get toDepartment parent
               Nothing -> return ()]

    let toPatientLayout = fill patientLayout
        toDepartmentLayout = shaped . center $ widget department

        outputToLayout = column 5 [shaped $ container toPatient toPatientLayout,
                                   center . alignTop $ container toDepartment toDepartmentLayout]
        outputDetailsLayout = column 3 [row 5 [center $ label "Izlaz na: ", hstretch $ widget outputTo],
                                        row 5 [outputToLayout, hglue, alignBottomRight . boxed "Datum" $ widget calendar]]

        inventoryOutputLeftLayout = vfill drugStockLayout

        inventoryOutputMiddleLayout =
            vstretch . center . column 2 $
                [vstretch $ vspace 1,
                 marginLeft . margin 3 . center $ label "Količina\n(jedinice)",
                 widget quantityEntry,
                 expand $ widget scheduleButton,
                 vstretch $ vspace 1,
                 alignBottom . expand $ widget submitButton]

        inventoryOutputRightLayout =
            vfill . column 5 $
                [center . row 5 . fmap widget $ [plusButton, deleteButton, minusButton],
                 listViewLayout transactionList,
                 outputDetailsLayout]

        inventoryOutputLayout =
            row 3 [inventoryOutputLeftLayout,
                   inventoryOutputMiddleLayout,
                   inventoryOutputRightLayout]

    return (void $ updatePatientList patient patientFilter patientSearch patientsVar,
            outputStockList, inventoryOutputLayout)

  where updatePatientList = filterListForce (overviewToEntityList patientOverview)

