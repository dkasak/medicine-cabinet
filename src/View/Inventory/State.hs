module View.Inventory.State where

import           Data.List.NonEmpty (NonEmpty (..), (<|))
import           Data.Ord
import qualified Data.Text          as T
import           Graphics.UI.WX     hiding (on, select)

import           Classes
import           Models
import           Stock
import           Utils
import           Widgets.TypedList

stockTypedListMeta :: TypedListMeta (PricedBatch Drug)
stockTypedListMeta = TypedListMeta
  {  typedListColumns = [ColumnDesc "Kom."
                                    (Just AlignCentre)
                                    (Just 70),
                         ColumnDesc (labelled DrugAtkCode)
                                    Nothing
                                    (Just 95),
                         ColumnDesc (labelled DrugPricePerPackage)
                                    Nothing
                                    (Just 110),
                         ColumnDesc (labelled DrugGenericName)
                                    Nothing
                                    (Just 180),
                         ColumnDesc (labelled DrugProprietaryName)
                                    Nothing
                                    (Just 200),
                         ColumnDesc (labelled DrugTypeDosePackaging)
                                    Nothing
                                    (Just 220),
                         ColumnDesc "Vrijednost"
                                    Nothing
                                    (Just 100)]
  , typedListFilters = TypedListFilter (T.pack $ labelled DrugGenericName)
                                        (\t -> T.isPrefixOf t . drugGenericName . pricedBatchItem)
                                        (comparing (drugGenericName . pricedBatchItem))
                                        Ascending <|
                       TypedListFilter (T.pack $ labelled DrugProprietaryName)
                                        (\t -> T.isPrefixOf t . drugProprietaryName . pricedBatchItem)
                                        (comparing (drugProprietaryName . pricedBatchItem))
                                        Ascending <|
                       TypedListFilter (T.pack $ labelled DrugAtkCode)
                                        (\t -> T.isPrefixOf t . drugAtkCode . pricedBatchItem)
                                        (comparing (drugAtkCode . pricedBatchItem))
                                        Ascending :| []
  , typedListDisplayRow = stockValues
  , typedListFetchValues = getStock
  }

stockValues :: PricedBatch Drug -> [String]
stockValues b = map ($ b)
    [ show . pricedBatchQuantity
    , labelled . drugAtkCode . pricedBatchItem
    , T.unpack . price . drugPricePerPackage . pricedBatchItem
    , labelled . drugGenericName . pricedBatchItem
    , labelled . drugProprietaryName . pricedBatchItem
    , labelled . drugTypeDosePackaging . pricedBatchItem
    , T.unpack . price . pricedBatchPrice]
