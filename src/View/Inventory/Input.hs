{-# LANGUAGE MultiParamTypeClasses #-}

module View.Inventory.Input where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Control   (MonadBaseControl)
import Data.Decimal
import Data.Maybe
import Database.Esqueleto            hiding (get, on, select, set, (*.))
import Graphics.UI.WX                hiding (sorted, when)
import Graphics.UI.WXCore.WxcClasses (CalendarCtrl)
import Graphics.UI.XTC               hiding (ListView)

import Classes
import Controls
import Controls.ListView
import Database
import Models
import Stock
import Utils
import View.Inventory.Misc
import View.TransactionHistory
import Widgets.EntityList
import Widgets.TypedList

scheduleSelectedForInput
    :: (Ord a, TypedValued Integer w)
    => w
    -> ListView (Entity a)
    -> ListView (Batch a)
    -> IO ()
scheduleSelectedForInput quantityEntry itemList transactionList =
    do quantity <- fromMaybe 1 <$> get quantityEntry typedValue
       set quantityEntry [typedValue := Just 1]
       itemsToSchedule <- listViewGetSelectedVals itemList
       scheduledItems <- listViewGetItems transactionList
       let itemsToSchedule' = (Batch quantity . entityVal)
                          <$> itemsToSchedule
           updatedDrugs = joinBatches $ itemsToSchedule' ++ scheduledItems
       listViewSetItems transactionList updatedDrugs

submitInputTransaction :: (MonadIO m, MonadBaseControl IO m)
                       => CalendarCtrl w -> ListView (Batch Drug) -> m ()
submitInputTransaction calendar transactionList = runDB $
    do day <- liftIO $ get calendar date
       today <- liftIO getToday
       tid <- insert $ Transaction Input day
       drugBatches <- liftIO $ listViewGetItems transactionList
       let drugs = batchItem <$> drugBatches
       mDrugs <- sequence $ fetchDrug <$> drugs
       let drugs = catMaybes mDrugs
           drugIds = map entityKey drugs
           lines = map (toInputTransactionLine tid) (zip drugIds drugBatches)

       -- All summaries since the date of the transaction are now invalid.
       invalidateSummariesSince day

       mapM_ insertMany_ $ chunks 150 lines

       -- Reset controls
       -- The calendar line is such because of a bug in WXHaskell.
       liftIO $ listViewSetItems transactionList []
       liftIO $ fixedDayToWXDate today >>= \d -> set calendar [date := d]

doSubmitInput :: ListView (Batch Drug)
              -> TypedList (PricedBatch Drug)
              -> TypedList (PricedBatch Drug)
              -> CalendarCtrl w
              -> HistoryView
              -> IO ()
doSubmitInput transactionList stockList outputStockList calendar history =
    do items <- listViewGetItems transactionList
       unless (null items) $ do
           submitInputTransaction calendar transactionList

           -- recalculate the stock and store it in a variable to avoid recalculation
           liftIO $ recalculateStock >>= setStock

           filterTypedList stockList
           filterTypedList outputStockList
           updateHistory history

toInputTransactionLine :: TransactionId -> (DrugId, Batch Drug) -> TransactionLine
toInputTransactionLine tid (drugid, Batch quantity drug) = line
    where line = TransactionLine tid drugid totalUnits totalPrice packagePrice
          totalUnits = packageUnits * Quantity quantity
          totalPrice = packagePrice * toDecimal quantity
          packageUnits = drugUnitsPerPackage drug
          packagePrice = drugPricePerPackage drug

          toDecimal :: Integral a => a -> Decimal
          toDecimal = roundTo 2 . fromIntegral

shortDrugEntityList :: EntityList Drug
shortDrugEntityList = EntityList
  { entityListColumns = [ColumnDesc (labelled DrugAtkCode)
                                    Nothing
                                    (Just 95),
                         ColumnDesc (labelled DrugGenericName)
                                    Nothing
                                    (Just 120),
                         ColumnDesc (labelled DrugProprietaryName)
                                    Nothing
                                    (Just 110),
                         ColumnDesc "Cijena"
                                    (Just AlignRight)
                                    (Just 70),
                         ColumnDesc (labelled DrugTypeDosePackaging)
                                    Nothing
                                    (Just 210)]
  , entityListFilters = [DrugGenericName, DrugProprietaryName, DrugAtkCode]
  , entityListDisplayRow = shortDrugValues
  }

shortDrugValues :: Entity Drug -> [String]
shortDrugValues ep = map (labelled . ($ drug))
    [ drugAtkCode
    , drugGenericName
    , drugProprietaryName
    , price . drugPricePerPackage
    , drugTypeDosePackaging]
    where drug = entityVal ep

inventoryInput :: GuiContext
               -> TypedList (PricedBatch Drug)
               -> TypedList (PricedBatch Drug)
               -> HistoryView
               -> IO Layout
inventoryInput ctx stockList outputStockList history = do
    let pane = guiPanel ctx

    rowsVar <- variable []

    (drugList, _, _, entityListLayout) <- entityList ctx shortDrugEntityList rowsVar
    transactionList <- listView' pane (entityListColumns batchEntityList) batchValues

    quantityEntry <- mkValueEntry pane [typedValue := Just 1, alignment := AlignCentre]
    scheduleButton <- button pane
        [text := "→",
         tooltip := "Dodaj u transakciju",
         on command := scheduleSelectedForInput quantityEntry drugList transactionList]
    plusButton <- button' (sz 40 30) pane
        [text := "+",
         tooltip := "Povećaj za 1",
         on command := updateSelectedQuantity (+1) transactionList]
    minusButton <- button' (sz 40 30) pane
        [text := "-",
         tooltip := "Umanji za 1",
         on command := updateSelectedQuantity (+ negate 1) transactionList]
    deleteButton <- button' (sz 40 30) pane
        [text := "×",
         tooltip := "Obriši",
         on command := listViewDeleteSelectedItems transactionList]

    calendar <- calendarCtrl pane []

    submitButton <- button pane [text := "Pohrani",
                                 on command := doSubmitInput transactionList stockList outputStockList calendar history]

    return $ row 3 [stretch . vfill $ entityListLayout,
                    vstretch . center . column 2 $
                        [vstretch $ vspace 1,
                         marginLeft . margin 3 . center $ label "Količina\n(pakiranja)",
                         widget quantityEntry,
                         expand $ widget scheduleButton,
                         vstretch $ vspace 1,
                         alignBottom . expand $ widget submitButton],
                    stretch . vfill . column 5 $
                        [center . row 5 . fmap widget $ [plusButton, deleteButton, minusButton],
                         listViewLayout transactionList,
                         center $ boxed "Datum" $ widget calendar]]
