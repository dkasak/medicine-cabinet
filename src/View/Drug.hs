module View.Drug where

import           Control.Applicative (ZipList (..))
import           Data.Decimal
import           Data.Maybe
import qualified Data.Text           as T
import           Database.Esqueleto  hiding (get, set)
import           Graphics.UI.WX      hiding (on, select)
import qualified Graphics.UI.WX      as W
import           Graphics.UI.XTC

import           Classes
import           Database
import           Models
import           Stock
import           Utils
import           Widgets.Form        (FormField (..), form, formAcceptButton,
                                      formCloseButton, setFormLayout, textField,
                                      valueField)
import           Widgets.Overview

drugOverview :: Overview Drug
drugOverview = Overview
  { overviewColumns = [ColumnDesc (labelled DrugAtkCode)
                                  Nothing
                                  (Just 95),
                       ColumnDesc (labelled DrugList)
                                  Nothing
                                  (Just 100),
                       ColumnDesc (labelled DrugGenericName)
                                  Nothing
                                  (Just 120),
                       ColumnDesc (labelled DrugProprietaryName)
                                  Nothing
                                  (Just 120),
                       ColumnDesc (labelled DrugManufacturer)
                                  Nothing
                                  (Just 180),
                       ColumnDesc "Cijena"
                                  (Just AlignRight)
                                  (Just 90),
                       ColumnDesc (labelled DrugUnitsPerPackage)
                                  (Just AlignRight)
                                  (Just 130),
                       ColumnDesc (labelled DrugUnitDose)
                                  (Just AlignRight)
                                  (Just 85),
                       ColumnDesc (labelled DrugTypeDosePackaging)
                                  Nothing
                                  (Just 340)]
  , overviewFilters = [DrugProprietaryName, DrugGenericName, DrugAtkCode, DrugManufacturer]
  , overviewDisplayRow = drugValues
  , overviewAddForm = drugForm Nothing
  , overviewEditForm = drugForm
  , overviewIsItemDeletable = Just $
        \(Entity drugId _) -> null <$>
                              runDB
                               (select $
                                from $ \(tl `InnerJoin` t) -> do
                                on (tl ^. TransactionLineTransaction ==. t ^. TransactionId)
                                where_ (tl ^. TransactionLineDrug ==. val drugId
                                     &&. t ^. TransactionType `in_` valList [Input, Output]))
  , overviewPostMutationAction = Just $ recalculateStock >>= setStock
  , overviewFormSize = sz 600 (-1)
  }

drugValues :: Entity Drug -> [String]
drugValues ep = map (labelled . ($ drug))
    [ drugAtkCode
    , T.pack . labelled . drugList
    , drugGenericName
    , drugProprietaryName
    , drugManufacturer
    , price . drugPricePerPackage
    , T.pack . labelled . drugUnitsPerPackage
    , T.pack . labelled . drugUnitDose
    , drugTypeDosePackaging ]
    where drug = entityVal ep

drugForm :: Maybe Drug -> Panel () -> IO (Maybe Drug)
drugForm drug frame = do
    form'              <- form frame
    fATK               <- textField form'
    fList              <- mkChoiceView form' []
    set fList [typedItems := [minBound :: DrugList ..]]
    fGenericName       <- textField form'
    fProprietaryName   <- textField form'
    fManufacturer      <- textField form'
    fPricePerPackage   <- valueField form'
    fUnitsPerPackage   <- valueField form'
    fUnitDose          <- textField form'
    fTypeDosePackaging <- textField form'

    let editMode = isJust drug

    when editMode $ do
        set fATK [enabled := False]
        set fPricePerPackage [enabled := False]

    maybe (return ()) (\d ->
        do set fATK [typedValue := Just $ drugAtkCode d]
           set fList [typedMaybeSelection := Just $ drugList d]
           set fGenericName [typedValue := Just $ drugGenericName d]
           set fProprietaryName [typedValue := Just $ drugProprietaryName d]
           set fManufacturer [typedValue := Just $ drugManufacturer d]
           set fPricePerPackage [typedValue := Just $ drugPricePerPackage d]
           set fUnitsPerPackage [typedValue := Just . (\(Quantity q) -> q) . drugUnitsPerPackage $ d]
           set fUnitDose [typedValue := Just . T.pack . labelled $ drugUnitDose d]
           set fTypeDosePackaging [typedValue := Just $ drugTypeDosePackaging d]) drug

    let fields = getZipList
               $ ZipList [ ValueField fATK
                         , ChoiceField fList
                         , ValueField fGenericName
                         , ValueField fProprietaryName
                         , ValueField fManufacturer
                         , ValueField fPricePerPackage
                         , ValueField fUnitsPerPackage
                         , ValueField fUnitDose
                         , ValueField fTypeDosePackaging]
             <*> ZipList (map columnName . overviewColumns $ drugOverview)

    submit <- formAcceptButton (isJust drug) form' fields
    close  <- formCloseButton form'

    let size = overviewFormSize drugOverview
    setFormLayout "Unos lijeka" form' fields size submit close

    let getDrugFromForm =
            do atkCode         <- get fATK typedValue
               genericName     <- get fGenericName typedValue
               proprietaryName <- get fProprietaryName typedValue
               manufacturer    <- get fManufacturer typedValue
               pricePerPackage <- get fPricePerPackage typedValue
               quantity        <- get fUnitsPerPackage typedValue
               dose            <- get fUnitDose typedValue
               description     <- get fTypeDosePackaging typedValue
               list            <- get fList typedMaybeSelection
               let drug = Drug <$> atkCode
                               <*> genericName
                               <*> proprietaryName
                               <*> manufacturer
                               <*> (realFracToDecimal 2 <$> pricePerPackage)
                               <*> (Quantity <$> quantity)
                               <*> (Dose <$> dose)
                               <*> description
                               <*> list
                   unique = UniqueDrug <$> atkCode <*> pricePerPackage
               return (drug, unique)

    showModal form' (\returning -> do
       set submit [W.on command := do
           (mdrug, munique) <- getDrugFromForm
           case munique of
              Just unique ->
                do alreadyExists <- runDB $ getBy unique
                   case alreadyExists of
                       Just _ ->
                           if editMode
                               then returning mdrug
                               else errorDialog form' "Greška" "Lijek već postoji!"
                       Nothing -> returning mdrug
              Nothing -> returning Nothing]
       set close [W.on command := returning Nothing])

drugView :: GuiContext -> IO Layout
drugView ctx = overview ctx drugOverview
