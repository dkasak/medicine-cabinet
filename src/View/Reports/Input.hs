module View.Reports.Input where

import           Data.Monoid ((<>))
import qualified Data.Text                  as T
import           Data.Time
import           Graphics.UI.WX
import           Graphics.UI.WXCore
import           System.IO
import           System.FilePath

import           Classes
import           Config
import           Controls
import           Graphics.UI.WX.Combinators
import           HtmlReport
import           Models
import           Stock
import           Utils

transactionColumns :: [ColumnDesc]
transactionColumns =
    [ColumnDesc "Kom."
                (Just AlignCentre)
                (Just 70),
     ColumnDesc (labelled DrugAtkCode)
                Nothing
                (Just 95),
     ColumnDesc (labelled DrugPricePerPackage)
                Nothing
                (Just 110),
     ColumnDesc (labelled DrugGenericName)
                Nothing
                (Just 180),
     ColumnDesc (labelled DrugProprietaryName)
                Nothing
                (Just 200),
     ColumnDesc (labelled DrugTypeDosePackaging)
                Nothing
                (Just 220),
     ColumnDesc "Vrijednost"
                Nothing
                (Just 100),
     ColumnDesc "Datum"
        (Just AlignCentre)
        (Just 130)]

transactionToValues :: (Transaction, PricedBatch Drug) -> [String]
transactionToValues t = map ($ t)
    [ show . pricedBatchQuantity . snd
    , labelled . drugAtkCode . pricedBatchItem . snd
    , T.unpack . price . drugPricePerPackage . pricedBatchItem . snd
    , labelled . drugGenericName . pricedBatchItem . snd
    , labelled . drugProprietaryName . pricedBatchItem . snd
    , labelled . drugTypeDosePackaging . pricedBatchItem . snd
    , T.unpack . price . pricedBatchPrice . snd
    , show . transactionDate . fst]

displayTransactions :: ListView (Transaction, PricedBatch Drug) -> (Day, Day) -> IO ()
displayTransactions transactionList (d1, d2) = do
    tl <- inputTransactionsAndBatchesFromTo d1 d2
    listViewSetItems transactionList tl

refreshInputList :: ListView (Transaction, PricedBatch Drug) -> CalendarCtrl () -> CalendarCtrl () -> IO ()
refreshInputList inputList c1 c2 = do
    d1 <- get c1 date
    d2 <- get c2 date
    displayTransactions inputList (d1, d2)

generateHtmlReport :: CalendarCtrl () -> CalendarCtrl () -> ListView (Transaction, PricedBatch Drug) -> IO ()
generateHtmlReport c1 c2 transactionList = do
    d1 <- get c1 date
    d2 <- get c2 date
    transactions <- listViewGetItems transactionList
    let filename = "ulazi_" <> showGregorian d1 <> "_" <> showGregorian d2 <> ".html"
    reportFile <- openFile (reportsDir </> filename) WriteMode
    hPutStr reportFile (inputsHtmlReport d1 d2 transactions)
    hClose reportFile

inputReportView :: Window a -> IO (IO (), Layout)
inputReportView page =
    do from  <- calendarCtrl page []
       to    <- calendarCtrl page []
       today <- get from date
       transactionList <- listView' page transactionColumns transactionToValues
       reportButton <- button page [text := "Stvori izvještaj",
                                    on command := generateHtmlReport from to transactionList
                                               >> infoDialog page "Izvještaj" "Izvještaj stvoren."]
       displayTransactions transactionList (today, today)
       batches <- fmap snd <$> listViewGetItems transactionList
       transactionsValue <- staticText page [text := T.unpack . price . valueOfBatches $ batches]
       calendarCtrlOnCalEvent from (onDateChange $ updateInputView from to transactionList transactionsValue)
       calendarCtrlOnCalEvent to (onDateChange $ updateInputView from to transactionList transactionsValue)
       return (updateInputView from to transactionList transactionsValue,
               row 3 [column 3 [vstretch $ boxed "Od" $ widget from,
                                vstretch $ boxed "Do" $ widget to,
                                vstretch . center $ widget reportButton],
                      column 2 [fill $ listViewLayout transactionList,
                                hstretch $ row 2 [label "Ukupno: ", widget transactionsValue]]])

    where updateInputView c1 c2 transactionList transactionsValue = do
              refreshInputList transactionList c1 c2
              transactions <- fmap snd <$> listViewGetItems transactionList
              set transactionsValue [text := T.unpack . price . valueOfBatches $ transactions]
