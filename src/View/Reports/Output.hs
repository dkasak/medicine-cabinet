module View.Reports.Output where

import           Data.Maybe
import           Data.Monoid ((<>))
import qualified Data.Text                  as T
import           Data.Time
import qualified Database.Persist           as P
import           Graphics.UI.WX
import           Graphics.UI.WXCore
import           System.IO
import           System.FilePath

import           Classes
import           Config
import           Controls
import           Database
import           Graphics.UI.WX.Combinators
import           HtmlReport
import           Models
import           Stock
import           Utils

transactionColumns :: [ColumnDesc]
transactionColumns =
    [ColumnDesc "Kom."
                (Just AlignCentre)
                (Just 70),
     ColumnDesc (labelled DrugAtkCode)
                Nothing
                (Just 95),
     ColumnDesc (labelled DrugPricePerPackage)
                Nothing
                (Just 110),
     ColumnDesc (labelled DrugGenericName)
                Nothing
                (Just 150),
     ColumnDesc (labelled DrugProprietaryName)
                Nothing
                (Just 180),
     ColumnDesc (labelled DrugTypeDosePackaging)
                Nothing
                (Just 200),
     ColumnDesc "Vrijednost"
                Nothing
                (Just 100),
     ColumnDesc "Izlaz na"
                Nothing
                (Just 100),
     ColumnDesc "Datum"
        (Just AlignCentre)
        (Just 100)]

transactionToValues :: ((Transaction, OutputSpecification), PricedBatch Drug) -> [String]
transactionToValues t = map ($ t)
    [ show . pricedBatchQuantity . snd
    , labelled . drugAtkCode . pricedBatchItem . snd
    , T.unpack . price . drugPricePerPackage . pricedBatchItem . snd
    , labelled . drugGenericName . pricedBatchItem . snd
    , labelled . drugProprietaryName . pricedBatchItem . snd
    , labelled . drugTypeDosePackaging . pricedBatchItem . snd
    , T.unpack . price . pricedBatchPrice . snd
    , snd . fst
    , show . transactionDate . fst . fst]

displayTransactions :: ListView ((Transaction, OutputSpecification), PricedBatch Drug) -> (Day, Day) -> IO ()
displayTransactions transactionList (d1, d2) = do
    tl <- outputTransactionsIdsAndBatchesFromTo d1 d2
    tl' <- mapM (\((t, tId), batches) ->
                  do outputSpec <- outputSpecFromTransaction tId
                     return ((t, outputSpec), batches)) tl
    listViewSetItems transactionList tl'

outputSpecFromTransaction :: TransactionId -> IO OutputSpecification
outputSpecFromTransaction tId = do
    mdetails <- runDB $ transactionDetailsOfV tId
    if isJust mdetails
       then do let details = fromJust mdetails
               case outputTransactionDetailsOutput details of
                  ToPatient -> do
                      let mPatientId = outputTransactionDetailsPatient details
                      case mPatientId of
                          Nothing -> return ""
                          Just patientId -> do
                              mpatient <- runDB $ P.get patientId
                              case mpatient of
                                  Nothing -> return ""
                                  Just patient -> return . T.unpack . patientDoctorName $ patient
                  ToDepartment -> do
                      let mdepartment = outputTransactionDetailsDepartment details
                      return $ maybe "" labelled mdepartment
                  ToTrash -> return "Otpad"
        else return ""

refreshOutputList :: ListView ((Transaction, OutputSpecification), PricedBatch Drug) -> CalendarCtrl () -> CalendarCtrl () -> IO ()
refreshOutputList outputList c1 c2 = do
    d1 <- get c1 date
    d2 <- get c2 date
    displayTransactions outputList (d1, d2)

generateHtmlReport :: CalendarCtrl () -> CalendarCtrl () -> ListView ((Transaction, OutputSpecification), PricedBatch Drug) -> IO ()
generateHtmlReport c1 c2 transactionList = do
    d1 <- get c1 date
    d2 <- get c2 date
    transactions <- listViewGetItems transactionList
    let filename = "izlazi_" <> showGregorian d1 <> "_" <> showGregorian d2 <> ".html"
    reportFile <- openFile (reportsDir </> filename) WriteMode
    hPutStr reportFile (outputsHtmlReport d1 d2 transactions)
    hClose reportFile

outputReportView :: Window a -> IO (IO (), Layout)
outputReportView page =
    do from  <- calendarCtrl page []
       to    <- calendarCtrl page []
       today <- get from date
       transactionList <- listView' page transactionColumns transactionToValues
       reportButton <- button page [text := "Stvori izvještaj",
                                    on command := generateHtmlReport from to transactionList
                                               >> infoDialog page "Izvještaj" "Izvještaj stvoren."]
       displayTransactions transactionList (today, today)
       batches <- fmap snd <$> listViewGetItems transactionList
       transactionsValue <- staticText page [text := T.unpack . price . valueOfBatches $ batches]
       calendarCtrlOnCalEvent from (onDateChange $ updateOutputView from to transactionList transactionsValue)
       calendarCtrlOnCalEvent to (onDateChange $ updateOutputView from to transactionList transactionsValue)
       return (updateOutputView from to transactionList transactionsValue,
               row 3 [column 3 [vstretch $ boxed "Od" $ widget from,
                                vstretch $ boxed "Do" $ widget to,
                                vstretch . center $ widget reportButton],
                      column 2 [fill $ listViewLayout transactionList,
                                hstretch $ row 2 [label "Ukupno: ", widget transactionsValue]]])

    where updateOutputView c1 c2 transactionList transactionsValue = do
              refreshOutputList transactionList c1 c2
              transactions <- fmap snd <$> listViewGetItems transactionList
              set transactionsValue [text := T.unpack . price . valueOfBatches $ transactions]
