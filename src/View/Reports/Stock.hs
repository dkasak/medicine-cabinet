module View.Reports.Stock where

import           Data.Monoid ((<>))
import qualified Data.Text                  as T
import           Data.Time
import           Graphics.UI.WX
import           Graphics.UI.WXCore
import           System.IO
import           System.FilePath

import           Classes
import           Config
import           Controls
import           HtmlReport
import           Graphics.UI.WX.Combinators
import           Models
import           Stock
import           Utils

stockListColumns :: [ColumnDesc]
stockListColumns = [ColumnDesc "Kom."
                               (Just AlignCentre)
                               (Just 70),
                    ColumnDesc (labelled DrugAtkCode)
                               Nothing
                               (Just 95),
                    ColumnDesc  (labelled DrugPricePerPackage)
                               Nothing
                               (Just 110),
                    ColumnDesc (labelled DrugGenericName)
                               Nothing
                               (Just 180),
                    ColumnDesc (labelled DrugProprietaryName)
                               Nothing
                               (Just 200),
                    ColumnDesc (labelled DrugTypeDosePackaging)
                               Nothing
                               (Just 220),
                    ColumnDesc "Vrijednost"
                               Nothing
                               (Just 100)]

stockValues :: PricedBatch Drug -> [String]
stockValues b = map ($ b)
    [ show . pricedBatchQuantity
    , labelled . drugAtkCode . pricedBatchItem
    , T.unpack . price . drugPricePerPackage . pricedBatchItem
    , labelled . drugGenericName . pricedBatchItem
    , labelled . drugProprietaryName . pricedBatchItem
    , labelled . drugTypeDosePackaging . pricedBatchItem
    , T.unpack . price . pricedBatchPrice]

displayStockOnDate :: ListView (PricedBatch Drug) -> Day -> IO ()
displayStockOnDate stockList date =
    recalculateStockOn date >>= listViewSetItems stockList

refreshStockList :: ListView (PricedBatch Drug) -> CalendarCtrl () -> IO ()
refreshStockList stockList calendar = get calendar date >>= displayStockOnDate stockList

generateHtmlReport :: CalendarCtrl () -> ListView (PricedBatch Drug) -> IO ()
generateHtmlReport calendar transactionList = do
    d <- get calendar date
    transactions <- listViewGetItems transactionList
    let filename = "stanje_" <> showGregorian d <> ".html"
    reportFile <- openFile (reportsDir </> filename) WriteMode
    hPutStr reportFile (stockHtmlReport d transactions)
    hClose reportFile

stockReportView :: Window a -> IO (IO (), Layout)
stockReportView page =
    do calendar  <- calendarCtrl page []
       today     <- get calendar date
       stockList <- listView' page stockListColumns stockValues
       reportButton <- button page [text := "Stvori izvještaj",
                                    on command := generateHtmlReport calendar stockList
                                               >> infoDialog page "Izvještaj" "Izvještaj stvoren."]
       displayStockOnDate stockList today
       stock <- listViewGetItems stockList
       stockValue <- staticText page [text := T.unpack . price . valueOfBatches $ stock]
       calendarCtrlOnCalEvent calendar (onDateChange $ updateStockView calendar stockList stockValue)
       return (updateStockView calendar stockList stockValue,
               row 3 [column 2 [vstretch $ boxed "Datum" $ widget calendar,
                                vstretch . center $ widget reportButton],
                      column 2 [fill $ listViewLayout stockList,
                                hstretch $ row 2 [label "Ukupno: ", widget stockValue]]])

    where updateStockView calendar stockList stockValue = do
              refreshStockList stockList calendar
              stock <- listViewGetItems stockList
              set stockValue [text := T.unpack . price . valueOfBatches $ stock]
