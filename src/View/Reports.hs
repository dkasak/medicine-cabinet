module View.Reports where

import Control.Monad
import Graphics.UI.WX
import Graphics.UI.WXCore         (windowFreeze, windowThaw)
import Graphics.UI.XTC

import Classes
import Graphics.UI.WX.Combinators
import Utils
import View.Reports.Input
import View.Reports.Output
import View.Reports.Stock

data ReportMode = InventoryOnDate
                | InputsOnDateRange
                | OutputsOnDateRange
                deriving (Bounded, Enum)

instance Label ReportMode where
    labelled InventoryOnDate    = "Stanje na datum"
    labelled InputsOnDateRange  = "Ulazi"
    labelled OutputsOnDateRange = "Izlazi"

switchScreens :: GuiContext
              -> ChoiceView ReportMode ()
              -> (Panel (), Panel (), Panel ()) -> IO ()
switchScreens ctx modeSelector (stockReportPage, inputReportPage, outputReportPage) =
    set modeSelector [on select :=
        do mode <- get modeSelector typedMaybeSelection
           void $ case mode of
               Just InventoryOnDate -> do
                   windowFreeze frame
                   showHide stockReportPage [inputReportPage, outputReportPage]
                   windowReFit reportsTab
                   windowThaw frame
               Just InputsOnDateRange -> do
                   windowFreeze frame
                   showHide inputReportPage [stockReportPage, outputReportPage]
                   windowReFit reportsTab
                   windowThaw frame
               Just OutputsOnDateRange -> do
                   windowFreeze frame
                   showHide outputReportPage [stockReportPage, inputReportPage]
                   windowReFit reportsTab
                   windowThaw frame
               Nothing -> return ()]
    where reportsTab = guiPanel ctx
          frame = guiFrame ctx

reportsView :: GuiContext -> IO (IO (), Layout)
reportsView ctx = do
    let reportsTab = guiPanel ctx

    modeSelector <- mkChoiceView reportsTab [typedItems := [(minBound :: ReportMode) ..],
                                             typedMaybeSelection := Just minBound]

    stockReportPage  <- panel reportsTab []
    inputReportPage  <- panel reportsTab []
    outputReportPage <- panel reportsTab []

    (doUpdateStockReport, stockReportLayout) <- stockReportView stockReportPage
    (doUpdateInputReport, inputReportLayout) <- inputReportView inputReportPage
    (doUpdateOutputReport, outputReportLayout) <- outputReportView outputReportPage

    showHide stockReportPage [inputReportPage, outputReportPage]

    switchScreens ctx modeSelector (stockReportPage, inputReportPage, outputReportPage)

    let reportsLayout = container reportsTab
                      $ margin 3
                      $ fill
                      $ column 5 [row 2 [hspace 4,
                                         center $ label "Tip izvještaja: ",
                                         marginTop . margin 2 . hfill $ widget modeSelector],
                                       fill $ container stockReportPage stockReportLayout,
                                       fill $ container inputReportPage inputReportLayout,
                                       fill $ container outputReportPage outputReportLayout]

    return (doUpdateStockReport
         >> doUpdateInputReport
         >> doUpdateOutputReport, reportsLayout)
