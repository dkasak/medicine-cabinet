{-# LANGUAGE MultiParamTypeClasses #-}

module View.TransactionHistory where

import           Control.Monad              (liftM, unless)
import           Data.Maybe
import qualified Data.Text                  as T
import           Data.Time
import           Database.Esqueleto         hiding (get, isNothing, on, select,
                                             set, (=.))
import qualified Database.Esqueleto         as E
import           Database.Persist           hiding (get, (==.))
import qualified Database.Persist           as P
import           Graphics.UI.WX
import           Graphics.UI.XTC            hiding (ListView)
import           Safe

import           Classes
import           Controls
import           Controls.ListView
import           Database
import           Graphics.UI.WX.Combinators
import           Models
import           Stock
import           Utils
import           View.Inventory.State       (stockTypedListMeta, stockValues)
import           Widgets.TypedList

data HistoryFilter = HistoryInputOutput
                   | HistoryInput
                   | HistoryOutput
                   | HistoryVoided
                   deriving (Show, Eq, Enum, Bounded)

instance Label HistoryFilter where
    labelled HistoryInputOutput = "Ulazne/izlazne transakcije"
    labelled HistoryInput       = "Ulazne transakcije"
    labelled HistoryOutput      = "Izlazne transakcije"
    labelled HistoryVoided      = "Stornirane transakcije"

instance Label TransactionLine where
    labelled tl = count ++ " " ++ drug
        where count = show $ transactionLineQuantity tl
              drug = show $ transactionLineDrug tl

historyFilterToPersistentFilter :: HistoryFilter -> [Filter Transaction]
historyFilterToPersistentFilter HistoryInputOutput = [TransactionType <-. [Input, Output, Summary]]
historyFilterToPersistentFilter HistoryInput = [(P.==.) TransactionType Input]
historyFilterToPersistentFilter HistoryOutput = [(P.==.) TransactionType Output]
historyFilterToPersistentFilter HistoryVoided = [TransactionType <-. [VoidedOutput, VoidedInput]]

data HistoryView = HistoryView
    { historyViewList :: ListView (Entity Transaction)
    , historyViewFilter :: ChoiceView HistoryFilter ()
    , historyViewDisplayFields :: (ValueEntry T.Text (), ValueEntry Day (), ValueEntry T.Text ())
    , historyViewOutputDetails :: HistoryOutputDetails
    , historyViewTransactionLinesList :: ListView (PricedBatch Drug)
    }

data HistoryOutputDetails = HistoryOutputDetails
    { historyOutputLabel :: StaticText ()
    , historyOutputField :: ValueEntry T.Text ()
    }

historyColumns :: [ColumnDesc]
historyColumns =
    [ColumnDesc "Tip transakcije"
        (Just AlignCentre)
        (Just 150),
     ColumnDesc "Datum"
        (Just AlignCentre)
        (Just 130)]

transactionToValues :: Entity Transaction -> [String]
transactionToValues t = map ($ t)
    [ labelled . transactionType . entityVal
    , show . transactionDate . entityVal]

currentHistory :: [Filter Transaction] -> IO [Entity Transaction]
currentHistory historyFilter =
    runDB $ selectList historyFilter [Desc TransactionDate, Desc TransactionId]

updateHistory :: HistoryView -> IO ()
updateHistory (HistoryView historyList selectorBox _ _ _) = do
    historyFilter <- fromMaybe HistoryInputOutput <$> get selectorBox typedMaybeSelection
    transactions <- currentHistory (historyFilterToPersistentFilter historyFilter)
    listViewSetItems historyList transactions

patientNameAndSurname :: PatientId -> IO String
patientNameAndSurname patientId = do
    results <- getNameAndSurname
    let nameAndSurname = headMay results
    return $ maybe "" formName nameAndSurname
    where formName = unwords . unpackName
          unpackName (name, surname) = map (T.unpack . unValue) [name, surname]
          getNameAndSurname =
            runDB $
            E.select $
            E.from $ \p -> do
            E.where_ (p ^. PatientId ==. val patientId)
            return (p ^. PatientForename, p ^. PatientSurname)

updateHistoryDetails :: HistoryView -> IO ()
updateHistoryDetails (HistoryView historyList
                                  _
                                  (typeField, dateField, priceField)
                                  (HistoryOutputDetails outputLabel outputField)
                                  batchList) = do
    mSelectedTransaction <- listViewGetSelectedVal historyList
    case mSelectedTransaction of
        Just t -> do let (tId, tVal) = (entityKey t, entityVal t)
                     transactionLines <- runDB $ transactionLinesOfV tId
                     batches <- runDB $
                                joinPricedBatches <$> mapM transactionLineToBatch transactionLines
                     let transactionValue = foldMap transactionLineValue transactionLines

                     set typeField [text := labelled $ transactionType tVal]
                     set dateField [typedValue := Just $ transactionDate tVal]
                     set priceField [text := T.unpack . price . show $ transactionValue]

                     listViewSetItems batchList batches

                     when (transactionType tVal == Output) $ do
                         mdetails <- runDB $ transactionDetailsOfV tId
                         unless (isNothing mdetails) $ do
                            let details = fromJust mdetails
                            case outputTransactionDetailsOutput details of
                                ToPatient -> do
                                    let mPatientId = outputTransactionDetailsPatient details
                                    case mPatientId of
                                        Nothing -> return ()
                                        Just patientId -> do
                                            name <- patientNameAndSurname patientId
                                            set outputLabel [text := "Na pacijenta"]
                                            set outputField [text := name]
                                ToDepartment -> do
                                    set outputLabel [text := "Na odjel"]
                                    let mdepartment = outputTransactionDetailsDepartment details
                                        department = maybe "" labelled mdepartment
                                    set outputField [text := department]
                                ToTrash -> do
                                    set outputLabel [text := "Na otpad"]
                                    set outputField [visible := False]
        Nothing -> resetFields
    where resetFields = do set typeField [text := ""]
                           set dateField [typedValue := Nothing, text := ""]
                           set priceField [text := ""]
                           set outputField [text := ""]
                           listViewSetItems batchList []

changeTransactionStatus :: (TransactionId -> TransactionType -> IO ()) -> HistoryView -> IO ()
changeTransactionStatus modifyAction historyView =
    do let historyList = historyViewList historyView
       mSelected <- listViewGetSelectedVal historyList
       when (isJust mSelected) $ do
          let selected = fromJust mSelected
              transaction = entityVal selected
              tId = entityKey selected
              tType = transactionType transaction
              tDate = transactionDate transaction

          modifyAction tId tType

          -- All later summaries are now invalid so drop them.
          runDB $ invalidateSummariesSince tDate

          updateHistory historyView
          updateHistoryDetails historyView

          recalculateStock >>= setStock

voidAction :: TransactionId -> TransactionType -> IO ()
voidAction tId tType =
    runDB $ case tType of
        Input -> P.update tId [TransactionType =. VoidedInput]
        Output -> P.update tId [TransactionType =. VoidedOutput]
        Summary -> do P.delete tId
                      P.deleteWhere [TransactionLineTransaction P.==. tId]
        -- Do nothing for already voided transactions
        VoidedInput -> return ()
        VoidedOutput -> return ()

unvoidAction :: TransactionId -> TransactionType -> IO ()
unvoidAction tId tType =
    runDB $ case tType of
        -- Do nothing for not yed voided transactions
        Input        -> return ()
        Output       -> return ()
        Summary      -> return ()
        VoidedInput  -> P.update tId [TransactionType =. Input]
        VoidedOutput -> P.update tId [TransactionType =. Output]

voidTransaction :: HistoryView -> IO ()
voidTransaction = changeTransactionStatus voidAction

unvoidTransaction :: HistoryView -> IO ()
unvoidTransaction = changeTransactionStatus unvoidAction

modifyVoidButton :: (TypedMaybeSelection HistoryFilter w, Commanding w1, Textual w1)
                 => w1
                 -> HistoryView
                 -> w
                 -> IO ()
modifyVoidButton b view selector = do
    mode <- liftM (fromMaybe (minBound :: HistoryFilter)) $ get selector typedMaybeSelection
    if mode == HistoryVoided
       then set b [text := "Aktiviraj",
                   on command := unvoidTransaction view]
       else set b [text := "Storniraj",
                   on command := voidTransaction view]

outputTransactionSelected :: ListView (Entity Transaction) -> IO Bool
outputTransactionSelected lv = do
    mselected <- listViewGetSelectedVal lv
    case mselected of
        Nothing -> return False
        Just eTransaction -> do mtransaction <- runDB $ P.get (entityKey eTransaction)
                                return $ maybe False ((== Output) . transactionType) mtransaction

fetchTransactionOutputDetails :: TransactionId -> IO [Entity OutputTransactionDetails]
fetchTransactionOutputDetails tId =
    runDB $
    E.select $
    E.from $ \(t `InnerJoin` o) -> do
    E.on (t ^. TransactionId ==. o ^. OutputTransactionDetailsTransaction)
    E.where_ (t ^. TransactionId ==. val tId)
    return o

outputTransactionTowardPatient :: ListView (Entity Transaction) -> IO Bool
outputTransactionTowardPatient lv = do
    mselected <- listViewGetSelectedVal lv
    case mselected of
        Nothing -> return False
        Just eTransaction ->
            do details <- map entityVal <$> fetchTransactionOutputDetails (entityKey eTransaction)
               if null details
                  then return False
                  else return $ transactionType (entityVal eTransaction) == Output
                             && isJust (outputTransactionDetailsPatient (headNote "toward patient" details))

outputTransactionTowardDepartment :: ListView (Entity Transaction) -> IO Bool
outputTransactionTowardDepartment lv = do
    mselected <- listViewGetSelectedVal lv
    case mselected of
        Nothing -> return False
        Just eTransaction ->
            do details <- map entityVal <$> fetchTransactionOutputDetails (entityKey eTransaction)
               if null details
                  then return False
                  else return $ transactionType (entityVal eTransaction) == Output
                             && isJust (outputTransactionDetailsDepartment (headNote "toward department" details))

historyView :: Window a -> IO (HistoryView, Layout)
historyView historyTab = do
    modeSelector <- mkChoiceView historyTab
                        [typedItems := [(minBound :: HistoryFilter) ..],
                         typedMaybeSelection := Just minBound]
    historyList <- listView' historyTab historyColumns transactionToValues

    tTransactionType <- textFieldEx historyTab [enabled := False]
    tTransactionDate <- mkValueEntry historyTab [enabled := False]
    tTransactionValue <- textFieldEx historyTab [enabled := False]

    transactionExtraPane <- panel historyTab []

    lOutput <- staticText transactionExtraPane [text := ""]
            >>= visibleWhen historyList outputTransactionSelected
    tOutput <- textFieldEx transactionExtraPane [enabled := False, bgcolor := white]
            >>= visibleWhen historyList outputTransactionSelected
    batchList <- listView' transactionExtraPane (typedListColumns stockTypedListMeta) stockValues

    let view = HistoryView historyList modeSelector
                   (tTransactionType, tTransactionDate, tTransactionValue)
                   (HistoryOutputDetails lOutput tOutput)
                   batchList

    voidButton <- button historyTab [text := "Storniraj",
                                     on command := voidTransaction view]
              >>= enabledWhen historyList listViewHasSingleSelection

    updateHistory view
    set modeSelector [on select := updateHistory view
                                >> modifyVoidButton voidButton view modeSelector]
    set historyList [on select := updateHistoryDetails view]

    set transactionExtraPane [layout := column 2 [
                                  hfill $ widget lOutput,
                                  hfill $ widget tOutput,
                                  fill $ listViewLayout batchList]]

    let selectorRow = row 2 [hspace 4,
                             center $ label "Prikaži: ",
                             marginTop . margin 2 . hfill $ widget modeSelector]
    let detailsColumn = stretch
                      $ column 2 [hfill $ label "Tip transakcije",
                                  hfill $ widget tTransactionType,
                                  hfill $ label "Datum transakcije",
                                  hfill $ widget tTransactionDate,
                                  hfill $ label "Promet",
                                  hfill $ widget tTransactionValue,
                                  fill $ layoutFromWindow transactionExtraPane]
    let transactionInfoRow = row 2 [column 2 [vfill $ listViewLayout historyList,
                                              center $ widget voidButton],
                                    margin 5 $
                                    boxed "Detalji transakcije" $
                                    margin 3 detailsColumn]

    return (view,
            margin 3 $
            fill $ column 5 [selectorRow,
                             transactionInfoRow])
