{-# LANGUAGE TemplateHaskell #-}

module Models.Department where

import Database.Persist.TH

import Classes

instance Label Department where
    labelled D1 = "Odjel 1"
    labelled D2 = "Odjel 2"
    labelled D3 = "Odjel 3"

data Department = D1 | D2 | D3
    deriving (Show, Read, Eq, Enum, Bounded)

derivePersistField "Department"

