{-# LANGUAGE TemplateHaskell #-}

module Models.DrugList where

import Database.Persist.TH

import Classes

instance Label DrugList where
    labelled Basic        = "Osnovna"
    labelled Supplemented = "Dopunska"
    labelled Other        = "Drugo"

data DrugList = Basic | Supplemented | Other
    deriving (Show, Read, Eq, Ord, Enum, Bounded)

derivePersistField "DrugList"

