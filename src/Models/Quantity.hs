{-# LANGUAGE TemplateHaskell #-}

module Models.Quantity where

import Database.Persist.TH

import Classes

instance Label Quantity where
    labelled (Quantity i) = show i

newtype Quantity = Quantity Integer
    deriving (Show, Read, Eq, Ord, Enum, Integral, Real, Num)

fromQuantity :: Integral a => Quantity -> a
fromQuantity (Quantity i) = fromInteger i

toQuantity :: Integral a => a -> Quantity
toQuantity = Quantity . toInteger

derivePersistField "Quantity"

