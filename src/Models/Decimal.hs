{-# LANGUAGE TemplateHaskell #-}

module Models.Decimal where

import Data.Decimal
import Database.Persist.TH

import Classes

instance Label Decimal where
    labelled = show

instance Monoid Decimal
    where mempty = Decimal 0 0
          mappend = (+)

derivePersistField "Decimal"

