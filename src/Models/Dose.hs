{-# LANGUAGE TemplateHaskell #-}

module Models.Dose where

import qualified Data.Text           as T
import           Database.Persist.TH

import           Classes

instance Label Dose where
    labelled (Dose d) = T.unpack d

newtype Dose = Dose T.Text
    deriving (Show, Read, Eq, Ord)

derivePersistField "Dose"

