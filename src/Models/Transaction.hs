{-# LANGUAGE TemplateHaskell #-}

module Models.Transaction where

import Database.Persist.TH

import Classes

data TransactionType = Input
                     | Output
                     | VoidedInput
                     | VoidedOutput
                     | Summary
    deriving (Show, Read, Eq, Enum, Bounded)

data TransactionOutput = ToPatient | ToDepartment | ToTrash
    deriving (Show, Read, Eq, Enum, Bounded)

type OutputSpecification = String

instance Label TransactionType where
    labelled Input        = "Ulaz"
    labelled Output       = "Izlaz"
    labelled VoidedInput  = "Stornirana (ulaz)"
    labelled VoidedOutput = "Stornirana (izlaz)"
    labelled Summary      = "Stanje"

instance Label TransactionOutput where
    labelled ToPatient    = "Pacijenta"
    labelled ToDepartment = "Odjel"
    labelled ToTrash      = "Otpad"

derivePersistField "TransactionType"
derivePersistField "TransactionOutput"

