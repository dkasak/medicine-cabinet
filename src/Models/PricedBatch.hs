{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies       #-}

module Models.PricedBatch where

import Data.Decimal
import Data.Function        (on)
import Data.List            (foldl1', groupBy, sortOn)
import Data.MonoTraversable
import Models.Batch

data PricedBatch a where
    PricedBatch :: (Eq a, Ord a) =>
          { pricedBatchQuantity :: Integer
          , pricedBatchItem :: a
          , pricedBatchPrice :: Decimal
          } -> PricedBatch a

deriving instance Show a => Show (PricedBatch a)
deriving instance Eq a => Eq (PricedBatch a)

instance Ord a => Ord (PricedBatch a) where
    (PricedBatch q1 i1 p1) `compare` (PricedBatch q2 i2 p2) =
        case i1 `compare` i2 of
            EQ -> case compare q1 q2 of
                    EQ -> compare p1 p2
                    x  -> x
            x -> x

type instance Element (PricedBatch a) = Integer

instance MonoFunctor (PricedBatch a) where
    omap f (PricedBatch q i p) = PricedBatch (f q) i p

-- Avoid using this directly since it's not total.
-- Use joinPricedBatches instead.
(++.) :: PricedBatch a -> PricedBatch a -> PricedBatch a
(PricedBatch q1 d1 p1) ++. (PricedBatch q2 d2 p2)
    | d1 == d2 = PricedBatch (q1 + q2) d1 (p1 + p2)
    | otherwise = error "Tried to ++. two PricedBatches with non-equal items."

negatePricedBatch :: PricedBatch a -> PricedBatch a
negatePricedBatch (PricedBatch q i p) = PricedBatch (negate q) i (negate p)

joinPricedBatches :: (Eq a, Ord a) => [PricedBatch a] -> [PricedBatch a]
joinPricedBatches
            = filter (\(PricedBatch q _ _) -> q /= 0)
            . fmap (foldl1' (++.))
            . groupBy ((==) `on` pricedBatchItem)
            . sortOn pricedBatchItem

toBatch :: PricedBatch a -> Batch a
toBatch (PricedBatch q i _) = Batch q i

toBatches :: [PricedBatch a] -> [Batch a]
toBatches = map toBatch
