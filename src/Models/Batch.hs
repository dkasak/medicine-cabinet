{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies       #-}

module Models.Batch where

import Data.Function        (on)
import Data.List            (foldl1', groupBy, sortOn)
import Data.MonoTraversable

data Batch a where
    Batch :: (Eq a, Ord a) =>
          { batchQuantity :: Integer
          , batchItem :: a
          } -> Batch a

deriving instance Show a => Show (Batch a)
deriving instance Eq a => Eq (Batch a)

instance Ord a => Ord (Batch a) where
    (Batch q1 i1) `compare` (Batch q2 i2) =
        case i1 `compare` i2 of
            EQ -> compare q1 q2
            x  -> x

type instance Element (Batch a) = Integer

instance MonoFunctor (Batch a) where
    omap f (Batch q i) = Batch (f q) i

-- Avoid using this directly since it's not total.
-- Use joinBatches instead.
(+.) :: Batch a -> Batch a -> Batch a
(Batch q1 d1) +. (Batch q2 d2) | d1 == d2 = Batch (q1 + q2) d1
                               | otherwise = error "Tried to +. two Batches with non-equal items."

negateBatch :: Batch a -> Batch a
negateBatch = omap negate

joinBatches :: (Eq a, Ord a) => [Batch a] -> [Batch a]
joinBatches = filter (\(Batch q _) -> q /= 0)
            . fmap (foldl1' (+.))
            . groupBy ((==) `on` batchItem)
            . sortOn batchItem

joinBatchGroups :: (Eq a, Ord a) => [Batch a] -> [Batch a] -> [Batch a]
joinBatchGroups a b = joinBatches $ a ++ b
