module Main where

import           Control.Monad           (liftM, void)
import qualified Data.ByteString.Lazy    as B
import           Data.Csv
import           Data.List               (sort, stripPrefix)
import           Data.Maybe
import           Data.Monoid             ((<>))
import           Data.Text               (unpack)
import           Data.Time
import           Data.Vector             (Vector)
import qualified Data.Vector             as V
import           Database.Persist.Sql
import           Graphics.UI.WX          hiding (Vector)
import           Safe
import           System.Console.GetOpt
import           System.Directory
import           System.Environment      (getArgs, withArgs)
import           System.Exit
import           System.FilePath

import           Config
import           Controls.Notebook
import           Database                (runDB)
import           DrugImport              ()
import           Models
import           Stock
import           Utils                   (GuiContext (..))
import           View.Doctor             (doctorView)
import           View.Drug               (drugView)
import           View.Inventory          (inventoryView)
import           View.Patient            (patientView)
import           View.Reports            (reportsView)
import           View.TransactionHistory (historyView)

data Flag = Help | ImportDrugs FilePath
          deriving Show

options :: [OptDescr Flag]
options =
    [ Option ['i'] ["import"] (ReqArg ImportDrugs "FILE") "import drugs from CSV file"
    , Option ['h'] ["help"] (NoArg Help) "print usage information" ]

main :: IO ()
main =
  do runDB (runMigration migrateTables)
     makeDatabaseBackupIfNeeded dataDir
     createDirectoryIfMissing True reportsDir
     args <- getArgs
     case getOpt Permute options args of
         (flags, [], []) -> mapM_ process flags
         (_, _, errors) -> putStr (concat errors <> usageInfo header options) >> exitFailure
     withArgs [] $ start mainScreen
  where header = unpack $ "Usage: " <> executableName <> " [OPTION...]"
        process :: Flag -> IO ()
        process (ImportDrugs filepath) = importDrugsFromFile filepath >> exitSuccess
        process Help = putStr (usageInfo header options) >> exitSuccess

makeDatabaseBackupIfNeeded :: FilePath -> IO ()
makeDatabaseBackupIfNeeded dataDir =
  do contents <- getDirectoryContents dataDir
     today <- fmap (localDay . zonedTimeToLocalTime) getZonedTime
     let databaseFiles = filter ((== databaseExtension) . takeExtension) contents
         names = takeBaseName <$> databaseFiles
         dates' = mapMaybe (stripPrefix (databaseBaseName <> "-")) names
         dates = sort $ mapMaybe readMay dates'
         mlatest = lastMay dates
     case mlatest of
         Just day -> when (diffDays today day >= 7) $
                        copyFile databasePath (dataDir </> databaseBackupFilename today)
         Nothing -> copyFile databasePath (dataDir </> databaseBackupFilename today)

databaseBackupFilename :: Day -> FilePath
databaseBackupFilename day = databaseBaseName <> "-" <> show day <> databaseExtension

importDrugsFromFile :: FilePath -> IO ()
importDrugsFromFile path = do
    contents <- B.readFile path
    let decoded = fmap snd (decodeByName contents)
        ds = either (const V.empty) id decoded :: Vector Drug
    void $ runDB $ V.mapM insert_ ds

mainScreen :: IO ()
mainScreen = do
    recalculateStock >>= setStock

    createSummaryTransaction

    f <- frame [text := unpack programName, visible := False]

    p <- panel f []
    n <- notebook p []
    let ctx = GuiContext f p

    patientPage   <- panel n []
    patientLayout <- patientView ctx {guiPanel = patientPage}

    doctorPage    <- panel n []
    doctorLayout  <- doctorView ctx {guiPanel = doctorPage}

    drugPage      <- panel n []
    drugLayout    <- drugView ctx {guiPanel = drugPage}

    historyPage <- panel n []
    (history, historyLayout) <- historyView historyPage

    inventoryPage <- panel n []
    (doUpdateStockLists, inventoryLayout) <- inventoryView ctx {guiPanel = inventoryPage} history

    reportsPage <- panel n []
    (doUpdateReports, reportsLayout) <- reportsView ctx {guiPanel = reportsPage}

    -- Run certain actions when some tabs are selected
    -- (mostly updates to GUI controls to reflect updates in the model)
    set n [on click ::= \nb p -> clickedPage nb p >>= \page ->
                            case page of
                                x | x == inventoryPageNum -> doUpdateStockLists
                                  | x == reportsPageNum   -> doUpdateReports
                                  | otherwise -> return ()]

    set f [layout :=
            margin 5    $
            container p $
            tabs n [ tab "Ljekarna"    (fill inventoryLayout)
                   , tab "Povijest"    (fill $ container historyPage historyLayout)
                   , tab "Lijekovi"    (fill $ container drugPage drugLayout)
                   , tab "Pacijenti"   (fill $ container patientPage patientLayout)
                   , tab "Liječnici"   (fill $ container doctorPage doctorLayout)
                   , tab "Izvještaji"  (fill reportsLayout)]]

    set f [visible := True]

inventoryPageNum :: Int
inventoryPageNum = 0

reportsPageNum :: Int
reportsPageNum = 5
