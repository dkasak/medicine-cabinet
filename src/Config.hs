module Config where

import Data.Monoid
import Data.Text       (Text)
import System.FilePath

programName :: Text
programName = "Medicine Cabinet"

authorEmail :: Text
authorEmail = "dkasak@termina.org.uk"

executableName :: Text
executableName = "medicine-cabinet"

dataDir :: FilePath
dataDir = "."

reportsDir :: FilePath
reportsDir = dataDir </> "izvjestaji"

databaseBaseName :: FilePath
databaseBaseName = "db"

databaseExtension :: String
databaseExtension = ".sqlite"

databasePath :: String
databasePath = dataDir </> (databaseBaseName <> databaseExtension)

