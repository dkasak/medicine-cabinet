module Utils where

import           Control.Monad      (MonadPlus, liftM, mplus, mzero)
import           Data.Decimal
import qualified Data.Text          as T
import           Data.Time
import           Graphics.UI.WX
import           Graphics.UI.WXCore (DateTime, dateTimeCreate, dateTimeSet)

import           Classes
import           Models

data GuiContext = GuiContext
    { guiFrame :: Frame ()
    , guiPanel :: Panel ()
    }

data ColumnDesc =
  ColumnDesc
  { columnName  :: String
  , columnAlign :: Maybe Align
  , columnWidth :: Maybe Int
  }

isPossibly :: Eq a => a -> (a -> Bool) -> Maybe a
x `isPossibly` p | not $ p x = Just x
                 | otherwise = Nothing

-- Applies function only if the result satisfies a predicate
ifResult :: (t -> Bool) -> (t -> t) -> t -> t
ifResult p f a = if p (f a) then f a else a

-- Applies function only if the result does not satisfy a predicate
unlessResult :: (t -> Bool) -> (t -> t) -> t -> t
unlessResult p = ifResult (not . p)

-- Merges two lists, replacing elements from the first list with elements
-- of the second when those two elements satisfy a predicate.
--
-- Example:
--     > zigzag (\x y -> x `mod` y == 0) [2, 7, 9, 12, 11] [3, 6, 7]
--     > [2, 7, 3, 6, 11, 7]
--
--  It be used to "update" a list using a partial sublist (with the same
--  order) of new equivalent elements. The second list can also contain new
--  elements which will get added at the end.
zigzag :: (a -> a -> Bool) -> [a] -> [a] -> [a]
zigzag p (x:xs) yss@(y:ys)
    | p x y     = y:zigzag p xs ys
    | otherwise = x:zigzag p xs yss
zigzag _ xs []    = xs
zigzag _ [] ys    = ys

-- Similar to zigzag, but instead of replacing the elements of the first list
-- with the matching elements from the second, it zips the matching elements.
--
-- Unlike zigzag, as soon as one of the lists runs out of elements, it stops
-- processing
--
-- Example:
--     > zipzag ((==) `on` length) ["aaa", "a", "aaaa", "aaaaa"] ["b", "bbbbb"]
--     > [("a","b"),("aaaaa","bbbbb")]
zipzag :: (a -> b -> Bool) -> [a] -> [b] -> [(a, b)]
zipzag p (x:xs) yss@(y:ys)
    | p x y     = (x, y):zipzag p xs ys
    | otherwise = zipzag p xs yss
zipzag _ _ _    = []

-- Yields the result of applying f until p holds, collecting results into
-- a MonadPlus instance (without the final one that invalidated p).
iterateUntilM' :: (Monad m, MonadPlus f) => (a -> Bool) -> (a -> m a) -> a -> m (f a)
iterateUntilM' p f v = go
    where go = do
            r <- f v
            if p r
                then return mzero
                else fmap (return r `mplus`) (iterateUntilM' p f r)

price :: Label a => a -> T.Text
price x = T.pack $ labelled x ++ " kn"

-- | Divide a @DecimalRaw@ value into one or more portions.  The portions
-- will be approximately equal, and the sum of the portions is guaranteed to
-- be the original value.
--
-- The portions are represented as a list of pairs.  The first part of each
-- pair is the number of portions, and the second part is the portion value.
-- Hence 10 dollars divided 3 ways will produce @[(2, 3.33), (1, 3.34)]@.
--
-- This is like the function in Data.Decimal, but using Integer instead of
-- Int.
divide' :: Decimal -> Integer -> [(Integer, Decimal)]
divide' (Decimal e n) d
    | d > 0 =
        case n `divMod` fromIntegral d of
          (result, 0) -> [(d, Decimal e result)]
          (result, r) -> [(d - fromIntegral r,
                           Decimal e result),
                          (fromIntegral r, Decimal e (result+1))]
    | otherwise = error "Data.Decimal.divide: Divisor must be > 0."

takeAtLeastHalf :: Integral t => t -> t -> (t, t)
takeAtLeastHalf total n =
     let half = n `div` 2 in
         if total >= half
             then (half, n - half)
             else (total, n - total)

fixedDayToWXDate :: Day -> IO (DateTime ())
fixedDayToWXDate day = do
    wxd <- dateTimeCreate
    dateTimeSet wxd d (m - 1) (fromInteger y) 0 0 0 0
    return wxd
    where (y,m,d) = toGregorian day

getToday :: IO Day
getToday = liftM (localDay . zonedTimeToLocalTime) getZonedTime

valueOfBatches :: [PricedBatch x] -> Decimal
valueOfBatches = foldMap pricedBatchPrice

chunks :: Int -> [a] -> [[a]]
chunks n = split
  where split [] = []
        split xs = take n xs : split (drop n xs)
