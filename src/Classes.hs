{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE UndecidableInstances #-}

module Classes where

import Data.Text       (Text, unpack)

import Graphics.UI.XTC (Labeled (..))

class Label x where
    labelled :: x -> String

instance Label String where
    labelled x = x

instance Label Text where
    labelled = unpack

instance Label x => Labeled x where
    toLabel = labelled

-- A class for WX widgets which have a primary event which we'd want to
-- react upon. For instance, for buttons this is probably the command
-- event, for lists (ListCtrl, ListView, etc) it is the selection event.
-- Used for combinators such as enabledWhen to save passing yet another
-- parameter and make the code more elegant. Therefore, this is purely
-- a practical class for this application not likely to be useful
-- elsewhere.
-- This is probably exactly what the new Updating class in wx is for, but
-- so far it has very little instances. It is also what XTC calls
-- Observable.
class Reactable a where
    react :: IO () -> a -> IO ()
