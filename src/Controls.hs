module Controls where

import           Data.Maybe
import qualified Data.Text                  as T
import           Graphics.UI.WX
import           Graphics.UI.XTC            (ValueEntry, mkValueEntry,
                                             mkValueEntryEx)

import           Graphics.UI.WX.Combinators
import           Utils

textField :: Window a -> IO (ValueEntry T.Text ())
textField = flip textFieldEx []

textFieldEx :: Window a -> [Prop (ValueEntry T.Text ())] -> IO (ValueEntry T.Text ())
textFieldEx f = mkValueEntryEx f T.unpack (Just . T.pack)

valueField :: (Read x, Show x) => Window a -> IO (ValueEntry x ())
valueField = flip mkValueEntry []

listView' :: Window b -> [ColumnDesc] -> (a -> [String]) -> IO (ListView a)
listView' parent cols toRow = do
  ctrl <- listCtrl parent [columns := map fromColumnDesc cols]
  var  <- variable [value := []]
  return $ ListView ctrl var toRow

fromColumnDesc :: ColumnDesc -> (String, Align, Int)
fromColumnDesc (ColumnDesc n a w) = (n, fromMaybe AlignLeft a, fromMaybe (-1) w)

-- Unresizeable button; works around a WxHaskell limitation which changes
-- size of buttons upon windowRe{Fit,Layout}*
button' :: Size -> Window a -> [Prop (Button ())] -> IO (Button ())
button' s p attrs = button p attrs >>= sized s
