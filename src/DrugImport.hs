module DrugImport where

import           Control.Applicative   (optional, (<|>))
import           Control.Monad         (mzero)
import qualified Data.Attoparsec.Text  as A
import qualified Data.ByteString.Char8 as B (unpack)
import           Data.Char             (isDigit)
import           Data.Csv
import           Data.Decimal
import           Data.Monoid           ((<>))
import           Data.Text             (Text)
import qualified Data.Text             as T
import           Safe                  (readMay)

import           Models

instance FromField Decimal where
    parseField s = maybe mzero pure (readMay . B.unpack $ s)

instance FromField DrugList where
    parseField s | s == "osnovna" = pure Basic
                 | s == "dopunska" = pure Supplemented
                 | otherwise = pure Supplemented

instance FromNamedRecord Drug where
  parseNamedRecord m =
    Drug <$> m .: "atk"
         <*> m .: "generic_name"
         <*> m .: "proprietary_name"
         <*> m .: "manufacturer"
         <*> pure totalPrice
         <*> quantity
         <*> dose
         <*> m .: "type_dose_packaging"
         <*> m .: "list"
    where totalPriceE = runParser (m .: "total_price" :: Parser Decimal)
          unitPriceE = runParser (m .: "unit_price" :: Parser Decimal)
          totalPrice = either (\_ -> error "No total price in record.") (roundTo 2) totalPriceE
          unitPrice = either (\_ -> error "No unit price in record.") (roundTo 2) unitPriceE
          divisible = totalPrice /= unitPrice
          quantity = parseQuantity divisible <$> m .: "type_dose_packaging"
          dose = parseDose divisible <$> m .: "type_dose_packaging"

parenContent :: A.Parser Text
parenContent = do
    A.char '('
    dose <- A.takeTill (== ')')
    A.char ')'
    return dose

parensDose :: A.Parser Dose
parensDose = do
    A.takeTill (== '(')
    dose <- parenContent
    return $ Dose dose

quantityTimesDose :: A.Parser (Integer, Text)
quantityTimesDose = do
    quantity <- A.decimal
    A.char 'x'
    dose <- A.takeText
    return (quantity, dose)

indivisibleDose :: A.Parser Dose
indivisibleDose = percentDose <|> parensDose <|> xDose
    where percentDose = do
              A.takeTill isDigit
              wholePercent <|> fractionalPercent
          wholePercent = do
              dose <- T.pack . show <$> (A.decimal :: A.Parser Integer)
              A.char '%'
              return $ Dose (dose <> "%")
          fractionalPercent = do
              d1 <- T.pack . show <$> (A.decimal :: A.Parser Integer)
              A.char ','
              d2 <- T.pack . show <$> (A.decimal :: A.Parser Integer)
              A.char '%'
              return $ Dose (d1 <> "," <> d2 <> "%")
          xDose = do
              A.takeTill isDigit
              (_, dose) <- quantityTimesDose
              return $ Dose dose

divisibleDose :: A.Parser Dose
divisibleDose = parensDose <|> xDose
    where xDose = do
              A.takeWhile (not . isDigit)
              (_, dose) <- quantityTimesDose
              return (Dose dose)

divisibleQuantity :: A.Parser Quantity
divisibleQuantity = do
    A.takeWhile (not . isDigit)
    units <- A.decimal
    A.char 'x'
    optional (A.char '(')
    A.takeTill (== ')')
    optional (A.char ')')
    return (Quantity units)

parseQuantity :: Bool -> Text -> Quantity
parseQuantity False _ = Quantity 1
parseQuantity True s = case A.parseOnly divisibleQuantity s of
    Left e -> error $ concat ["Error while parsing quantity (", e, ", ", T.unpack s, ")"]
    Right x -> x

parseDose :: Bool -> Text -> Dose
parseDose False s = case A.parseOnly indivisibleDose s of
    Left e -> error $ concat ["Error while parsing dose (", e, ", ", T.unpack s, ")"]
    Right x -> x
parseDose True s = case A.parseOnly divisibleDose s of
    Left e -> error $ concat ["Error while parsing dose (", e, ", ", T.unpack s, ")"]
    Right x -> x
